module gitee.com/ichub/webcli

go 1.13

replace (
	github.com/gin-gonic/gin => github.com/gin-gonic/gin v1.6.0
	github.com/gookit/goutil => github.com/gookit/goutil v0.2.0
	github.com/sirupsen/logrus => github.com/sirupsen/logrus v1.6.0
	github.com/spf13/viper => github.com/spf13/viper v1.4.0
	github.com/stretchr/testify => github.com/stretchr/testify v1.7.1
	google.golang.org/grpc => google.golang.org/grpc v1.26.0
)

require (
	gitee.com/cristiane/micro-mall-api v1.1.2
	github.com/dustin/go-humanize v1.0.1 // indirect
	github.com/emirpasic/gods v1.18.1
	github.com/fortytw2/leaktest v1.3.0 // indirect
	github.com/gin-gonic/gin v1.6.3
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gookit/goutil v0.0.0-00010101000000-000000000000
	github.com/imroc/req v0.3.2
	github.com/jinzhu/copier v0.4.0
	github.com/jinzhu/gorm v1.9.16
	github.com/json-iterator/go v1.1.12
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/micro/go-micro/v2 v2.9.1
	github.com/olivere/elastic v6.2.37+incompatible
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/sirupsen/logrus v1.4.2
	github.com/spf13/cobra v0.0.3
	github.com/spf13/viper v0.0.0-00010101000000-000000000000
	github.com/stretchr/testify v1.6.1
	gopkg.in/ini.v1 v1.67.0
)
