package page

import (
	"fmt"
	"gitee.com/ichub/webcli/goconfig/base/baseconsts"
	"gitee.com/ichub/webcli/goconfig/base/basedto"
	"gitee.com/ichub/webcli/goconfig/base/baseutils"
	"gitee.com/ichub/webcli/goconfig/base/baseutils/jsonutils"
	"gitee.com/ichub/webcli/goconfig/base/baseutils/stringutils"
	"gitee.com/ichub/webcli/goconfig/dbcontent"
	"gitee.com/ichub/webcli/goconfig/ichubcontext"
	"gitee.com/ichub/webcli/goconfig/ichublog"
	"github.com/jinzhu/gorm"
	"github.com/sirupsen/logrus"
)

/*
@Title    文件名称: page_request.go
@Description  描述:  PageRequest

@Author  作者: leijianming@163.com  时间(2024-02-21 22:38:21)
@Update  作者: leijianming@163.com  时间(2024-02-21 22:38:21)
*/
const (
	OrderAsc  = "asc"
	OrderDesc = "desc"
)

type PageRequest struct {
	basedto.BaseEntity `json:"-"`

	//每页记录数
	PageSize int `json:"page_size"`
	//当前页码
	PageCurrent int `json:"current"`
	//排序字段数组
	OrderBys []*OrderByDto `json:"order_by"`
	//查询字段条件
	Fields []*QueryField `json:"fields"`

	//Param interface{} `json:"param,omitempty"`
}

func NewPageRequest(pageSize int, pageCurrent int) *PageRequest {
	var pageRequest = &PageRequest{
		PageSize:    pageSize,
		PageCurrent: pageCurrent,

		OrderBys: []*OrderByDto{},
		Fields:   make([]*QueryField, 0),
	}

	pageRequest.InitProxy(pageRequest)
	return pageRequest
}

func NewIchubPageRequest() *PageRequest {

	return NewPageRequest(
		baseconsts.PAGE_SIZE_DEFAULT,
		baseconsts.PAGE_CURRENT)
}
func (this *PageRequest) Clear() {
	this.PageSize = baseconsts.PAGE_SIZE_DEFAULT
	this.PageCurrent = baseconsts.PAGE_CURRENT

	this.OrderBys = []*OrderByDto{}
	this.Fields = make([]*QueryField, 0)
}

func (this *PageRequest) GetDB() *gorm.DB {

	return dbcontent.GetDB()
}

func (this *PageRequest) InitPage() {
	if this.PageSize <= baseconsts.PAGE_SIZE_ZERO {

		this.PageSize = baseconsts.PAGE_SIZE_DEFAULT

	} else if this.PageSize > baseconsts.PAGE_SIZE_MAX {

		this.PageSize = baseconsts.PAGE_SIZE_MAX

	}
	if this.PageCurrent <= 0 {
		this.PageCurrent = baseconsts.PAGE_CURRENT
	}

}
func (this *PageRequest) FindFieldSign(op int) FieldSign {
	return OpSign[op]

}

func (this *PageRequest) Start() int {
	return (this.PageCurrent - 1) * this.PageSize
}
func (this *PageRequest) Limit() int {
	return this.PageSize
}

func (this *PageRequest) keySnake(key string) string {
	return stringutils.Camel2Case(key)
}
func (this *PageRequest) CheckTyope(field string, value interface{}) {
	baseutils.CheckType(value)
}

func (this *PageRequest) QueryFields(f string, opType int, opValues []interface{}) *PageRequest {

	var field = NewFields(this.keySnake(f), opType, opValues)
	this.Fields = append(this.Fields, field)

	return this
}

func (this *PageRequest) Eq(field string, opValues interface{}) *PageRequest {

	return this.QueryFields(field, Eq, []interface{}{opValues})
}
func (this *PageRequest) Ge(field string, opValue interface{}) *PageRequest {

	return this.QueryFields(field, Ge, []interface{}{opValue})
}
func (this *PageRequest) Le(field string, opValue interface{}) *PageRequest {

	return this.QueryFields(field, Le, []interface{}{opValue})
}
func (this *PageRequest) Lt(field string, opValue interface{}) *PageRequest {

	return this.QueryFields(field, Lt, []interface{}{opValue})
}
func (this *PageRequest) Gt(field string, opValue interface{}) *PageRequest {

	return this.QueryFields(field, Gt, []interface{}{opValue})
}
func (this *PageRequest) In(field string, opValues []interface{}) *PageRequest {

	return this.QueryFields(field, In, opValues)
}

func (this *PageRequest) NotIn(field string, opValues []interface{}) *PageRequest {

	return this.QueryFields(field, NotIn, opValues)
}
func (this *PageRequest) Like(field string, opValue interface{}) *PageRequest {

	return this.QueryFields(field, Like, []interface{}{opValue})
}

func (this *PageRequest) NotLike(field string, opValue interface{}) *PageRequest {

	return this.QueryFields(field, NotLike, []interface{}{opValue})
}
func (this *PageRequest) Between(field string, opValues []interface{}) *PageRequest {

	return this.QueryFields(field, Between, opValues)
}
func (this *PageRequest) NotBetween(field string, opValues []interface{}) *PageRequest {

	return this.QueryFields(field, NotBetween, opValues)
}
func (this *PageRequest) OrderBy(field string, sort string) *PageRequest {

	var orderby = OrderBy(field, sort)
	this.OrderBys = append(this.OrderBys, orderby)
	return this
}

func (this *PageRequest) OrderByAsc(field string) *PageRequest {

	return this.OrderBy(field, OrderAsc)
}
func (this *PageRequest) OrderByDesc(field string) *PageRequest {

	return this.OrderBy(field, OrderDesc)
}

func (this *PageRequest) ToMap() (*map[string]interface{}, error) {
	var str = jsonutils.ToJsonPretty(this)
	m, err := jsonutils.MapFromJson(str)
	if err != nil {
		logrus.Error(err)
	}
	return m, err

}

//update

func (this *PageRequest) FindByTable(table string, result interface{}) error {

	dbc := this.GetDB().Table(table)
	dbc = this.BuildWhere(dbc)
	dbc = this.Order(dbc)
	dbc = this.SetLimit(dbc)
	dbc = dbc.Find(result)

	return dbc.Error
}

func (daoInst *PageRequest) Insert(model interface{}) (interface{}, error) {

	err := dbcontent.GetDB().Create(model).Error
	if err != nil {
		logrus.Error(err.Error())

	}

	return model, err
}
func (daoInst *PageRequest) Update(model interface{}, pkey int64) (interface{}, error) {

	err := dbcontent.GetDB().Model(model).Where("rule_id=?", pkey).Updates(model).Error
	if err != nil {
		logrus.Error(err.Error())
		return -1, err
	}
	return model, err

}

func (this *PageRequest) FindBy(model interface{}, result interface{}) error {

	dbc := this.GetDB().Model(model)
	dbc = this.BuildWhere(dbc)
	dbc = this.Order(dbc)
	dbc = this.SetLimit(dbc)
	dbc = dbc.Find(result)

	return dbc.Error
}

func (this *PageRequest) BuildWhere(dbc *gorm.DB) *gorm.DB {

	this.InitPage()
	if this.Fields == nil {
		return dbc
	}
	for _, b := range this.Fields {

		if b.OpType == OpSign[Between] {
			dbc = dbc.Where(fmt.Sprintf("%s BETWEEN ? and ?", b.Field),
				b.Values[0], b.Values[1])
		}
		if b.OpType == OpSign[NotBetween] {
			dbc = dbc.Where(fmt.Sprintf("%s Not BETWEEN ? and ?", b.Field),
				b.Values[0], b.Values[1])
		}

		if b.OpType == OpSign[Ge] {
			dbc = dbc.Where(fmt.Sprintf("%s >= ?", b.Field), b.Values[0])
		}
		if b.OpType == OpSign[Gt] {
			dbc = dbc.Where(fmt.Sprintf("%s > ?", b.Field), b.Values[0])
		}
		if b.OpType == OpSign[Le] {
			dbc = dbc.Where(fmt.Sprintf("%s <= ?", b.Field), b.Values[0])
		}
		if b.OpType == OpSign[Lt] {
			dbc = dbc.Where(fmt.Sprintf("%s < ?", b.Field), b.Values[0])
		}
		if b.OpType == OpSign[Eq] {
			dbc = dbc.Where(fmt.Sprintf("%s = ?", b.Field), b.Values[0])
		}
		if b.OpType == OpSign[Ne] {
			dbc = dbc.Where(fmt.Sprintf("%s != ?", b.Field), b.Values[0])
		}
		if b.OpType == OpSign[In] {

			dbc = dbc.Where(fmt.Sprintf("%s in (%s)", b.Field, b.Values2InStr()))
		}
		if b.OpType == OpSign[NotIn] {
			dbc = dbc.Where(fmt.Sprintf("%s not in (%s)", b.Field, b.Values2InStr()))
		}
		if b.OpType == OpSign[Like] {
			var sval = baseutils.Any2Str(b.Values[0])
			dbc = dbc.Where(fmt.Sprintf("%s like ?", b.Field), "'%"+sval+"%'")
		}
		if b.OpType == OpSign[NotLike] {
			var sval = baseutils.Any2Str(b.Values[0])
			dbc = dbc.Where(fmt.Sprintf("%s not like ?", b.Field), "'%"+sval+"%'")
		}

	}

	return dbc
}

func (this *PageRequest) SetLimit(dbc *gorm.DB) *gorm.DB {
	this.InitPage()
	return dbc.Offset(this.Start()).Limit(this.Limit())

}
func (this *PageRequest) Order(dbc *gorm.DB) *gorm.DB {
	if len(this.OrderBys) > 0 {

		for _, orderBy := range this.OrderBys {
			dbc = dbc.Order(orderBy.ToOrderBy())
		}
	}
	return dbc
}

func (this *PageRequest) Count(model interface{}) (int, error) {
	dbc := dbcontent.GetDB().Model(model)
	dbc = this.BuildWhere(dbc).Offset(0).Limit(1)
	var count int
	if err := dbc.Count(&count).Error; err != nil {
		logrus.Error(err)
		return 0, err
	}

	logrus.Info("\ncount=", count)
	return count, nil

}
func (this *PageRequest) CountTable(table string) (int, error) {
	dbc := this.GetDB().Table(table)
	dbc = this.BuildWhere(dbc).Offset(0).Limit(1)
	var count int
	if err := dbc.Count(&count).Error; err != nil {
		logrus.Error(err)
		return 0, err
	}

	logrus.Info("\ncount=", count)
	return count, nil

}
func (this *PageRequest) QueryTable(table string, models interface{}) *IchubPageResult {

	fileName := table + "_pagerequest.json"
	ichubcontext.CommonContext.WriteDaoFile(fileName, this.ToPrettyString())
	this.InitPage()

	count, err := this.CountTable(table)
	if err != nil {
		logrus.Error(err)
		return NewPageResultError(err.Error())
	}
	if count > 0 {
		err = this.FindByTable(table, models)
		if err != nil {
			logrus.Error(err)
			return NewPageResultError(err.Error())
		}
	}
	var result = ValueOf(this)
	result.Total = count
	result.Data = models
	fileName = table + "_pageresult.json"
	ichubcontext.CommonContext.WriteDaoFile(fileName, result.ToPrettyString())

	ichublog.Log(result)
	return result
}
func (this *PageRequest) Query(model interface{}, models interface{}) *IchubPageResult {

	fileName := stringutils.NameOfType(model) + "_pagerequest.json"
	ichublog.IchubLog.Println(this.ToPrettyString())
	ichubcontext.CommonContext.WriteDaoFile(fileName, this.ToPrettyString())
	this.InitPage()

	count, err := this.Count(model)
	if err != nil {
		logrus.Error(err)
		return NewPageResultError(err.Error())
	}
	if count > 0 {
		err = this.FindBy(model, models)
		if err != nil {
			logrus.Error(err)
			return NewPageResultError(err.Error())
		}
	}
	var result = ValueOf(this)
	result.Total = count
	result.Data = models
	fileName = stringutils.NameOfType(model) + "_pageresult.json"
	ichubcontext.CommonContext.WriteDaoFile(fileName, result.ToPrettyString())

	ichublog.IchubLog.Println(result)
	return result
}

func (this *PageRequest) FindById(model interface{}, key int64) (bool, error) {

	db := this.GetDB().First(model, key)
	return db.RecordNotFound(), db.Error
}
