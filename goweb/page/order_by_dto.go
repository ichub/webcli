package page

import (
	"gitee.com/ichub/webcli/goconfig/base/baseutils/stringutils"
)

type OrderByDto struct {
	Field string `json:"field"`
	Sort  string `json:"sort"` //desc  asc

}

func (this *OrderByDto) keySnake(key string) string {
	return stringutils.Camel2Case(key)
}
func NewOrderByDto() *OrderByDto {
	return &OrderByDto{}
}

func (this *OrderByDto) ToOrderBy() string {
	return this.keySnake(this.Field) + " " + this.Sort
}

func OrderBy(field, sort string) *OrderByDto {
	return &OrderByDto{Field: field, Sort: sort}
}

func (this *OrderByDto) SetSort(sort string) {
	this.Sort = sort
}

func (this *OrderByDto) SetField(field string) {
	this.Field = field
}
