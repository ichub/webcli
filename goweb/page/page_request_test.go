package page

import (
	"gitee.com/ichub/webcli/goconfig/base/baseutils/stringutils"
	"gitee.com/ichub/webcli/goconfig/ichublog"
	"github.com/stretchr/testify/assert"
	"testing"
)

func init() {
	ichublog.InitLogrus()
}

type User struct {
	Id   int    `json:"id"`
	Name string `json:"name"`
}
type PageRequestUser struct {
	PageRequest
	param User
}

func NewPageRequestUser() *PageRequestUser {
	var pr = &PageRequestUser{}
	return pr
}
func Test0002_QueryRuleTable(t *testing.T) {
	var pageRequest = NewIchubPageRequest()

	pageRequest.OrderBy("ruleId", "desc")
	//	pageRequestBase.InFields("ruleId", []string{"1", "2221"})
	pageRequest.Eq("ruleId", 1)

	r := []map[string]interface{}{
		{"rule_id": 1},
	}
	var pageResult = pageRequest.QueryTable("rules", &r)

	assert.Equal(t, 200, pageResult.Code)
	//	for _, v := range *pageResult.Data.(*[]model.Rules) {
	//	logrus.Info(v.ToPrettyString())
	//	}
}

func Test0003_QueryRule(t *testing.T) {
	var pageRequest = NewPageRequest(3, 1)

	pageRequest.OrderBy("ruleId", "desc")
	pageRequest.Ge("ruleId", 1)

}

func Test0004_FindById(t *testing.T) {

}

func Test0005_QueryDept(t *testing.T) {
	var pageRequest = NewPageRequest(3, 1)

	pageRequest.OrderBy("DeptId", "asc") //	pageRequestBase.InFields("ruleId", []string{"1", "2221"})
	pageRequest.Ge("DeptId", 1)
	pageRequest.Gt("CreateTime", 0)
	var t1 = stringutils.Strtime2Time("2020-01-01 01:01:01")
	pageRequest.Gt("CreateTime", t1)
	pageRequest.Gt("CreateTime", "2011-01-01 01:01:01")
	pageRequest.Between("CreateTime", []interface{}{"2011-01-01 01:01:01", "2031-01-01 01:01:01"})
	pageRequest.Between("DeptId", []interface{}{100, 102})
	pageRequest.In("DeptId", []interface{}{"100", "102"})
	pageRequest.In("DeptId", []interface{}{100, 102})
	//pageRequestBase.Param2Between()

}

//ReflectTags

func Test0009_SelectRule(t *testing.T) {
	//fields
	//var dbinst = database.IniDb("mysql")
	//var rs, err = dbinst.Raw("select * from rules").Rows()
	//rs, err = dbinst.Table("rules").Order("rule_id asc").Limit(2).Rows()
	//if err != nil {
	//	logrus.Error(err)
	//}
	//
	//var this = metadata.NewIchubRecords().ScanRows(rs)
	//logrus.Info(this.ToPrettyString())
	//ichublog.Log(this.ToPrettyString())
}
