package page

import (
	"gitee.com/ichub/webcli/goconfig/base/basedto"
	"gitee.com/ichub/webcli/goconfig/base/baseutils"
)

type FieldSign string

const (
	Eq = iota
	Ne
	Ge
	Gt
	Le
	Lt
	In
	Like
	Between
	NotIn
	NotLike
	NotBetween
)

var OpSign = map[int]FieldSign{
	Eq: "=",
	Ne: "!=",
	Ge: ">=",
	Gt: ">",
	Le: "<=",
	Lt: "<",

	In:         "in",
	Like:       "like",
	Between:    "between",
	NotIn:      "not in",
	NotLike:    "not like",
	NotBetween: "not between",
}

type QueryField struct {
	basedto.BaseEntity
	Field string `json:"field"`

	OpType FieldSign     `json:"op_type"`
	Values []interface{} `json:"values"`
}

func NewQueryFields() *QueryField {
	var bfields = &QueryField{}
	bfields.InitProxy(bfields)
	return bfields
}
func NewFields(field string, opType int, opValues []interface{}) *QueryField {

	var bfields = &QueryField{
		Field:  field,
		OpType: OpSign[opType],
		Values: opValues,
	}
	bfields.InitProxy(bfields)
	return bfields
}
func (this *QueryField) CheckType() string {
	return baseutils.CheckType(this.Values[0])
}

func (this *QueryField) SetField(field string) {
	this.Field = field
}

func (this *QueryField) Values2InStr() string {
	var s = ""
	for _, v := range this.Values {
		if len(s) > 0 {
			s += ","
		}
		if this.CheckType() == "string" {
			s += `"` + baseutils.Any2Str(v) + `"`
		} else {
			s += baseutils.Any2Str(v)
		}

	}
	return s

}
