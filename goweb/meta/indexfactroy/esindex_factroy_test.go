package indexfactroy

import (
	"gitee.com/ichub/webcli/goconfig/base/meta/metadatafactroy"
	"gitee.com/ichub/webcli/goweb/pagees"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"testing"
)

var factry = metadatafactroy.NewMetadataFactroy()
var req = pagees.New()

func Test0001_CreateIndexFromDb(t *testing.T) {

	var table = factry.FindMetadata("rule_policy")
	logrus.Info(table, table.ToMappingStr())

	b, err := req.EsCreateIndex(table.IndexName, table.ToMappingStr())
	logrus.Info(b, err)
	assert.Equal(t, true, b)
}
