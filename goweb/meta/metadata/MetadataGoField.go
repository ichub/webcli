package metadata

import "gitee.com/ichub/webcli/goconfig/base/basedto"

/*
@Title    文件名称: metadataGoField.go
@Description  描述:  metadataGoField

@Author  作者: leijianming@163.com  时间(2024-02-21 22:38:21)
@Update  作者: leijianming@163.com  时间(2024-02-21 22:38:21)
*/
type MetadataGoField struct {
	basedto.BaseEntity
	ColumnName string `json:"column_name"`

	DataType   string `json:"data_type"`
	ColumnType string `json:"column_type"`
	GoType     string `json:"go_type"`
}

func NewMetadatGoField() *MetadataGoField {
	var goField = &MetadataGoField{}
	goField.InitProxy(goField)
	return goField
}
