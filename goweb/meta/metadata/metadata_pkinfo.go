package metadata

import "encoding/json"

/*
   @Title    文件名称: metadata_pk_info.go
   @Description  描述: 元数据--主键

   @Author  作者: leijianming@163.com  时间(2024-01-31 22:38:21)
   @Update  作者: leijianming@163.com  时间(2024-01-31 22:38:21)

*/
//for postgres
type MetadataPkInfo struct {
	PkName   string `gorm:"column:pkname"`
	ColName  string `gorm:"column:colname"`
	TypeName string `gorm:"column:typename"`
}

func NewMetadataPkInfo() *MetadataPkInfo {
	return &MetadataPkInfo{}
}

func (pkInfo *MetadataPkInfo) String() string {
	s, _ := json.Marshal(pkInfo)
	return string(s)
}

func (pkInfo *MetadataPkInfo) ToString() string {
	s, _ := json.MarshalIndent(pkInfo, "", "    ")
	return string(s)
}
