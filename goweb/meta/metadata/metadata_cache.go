package metadata

import (
	"gitee.com/ichub/webcli/goconfig/ichubcache"
	"time"
)

var InstMetadataCache = NewMetadataCache()

type MetadataCache struct {
	cache *ichubcache.IchubCache
}

func NewMetadataCache() *MetadataCache {
	return &MetadataCache{
		cache: ichubcache.NewIchubCache("RuleEngine:"),
	}
}

func (this *MetadataCache) CacheGet(tableName string) (*MetadataTable, bool) {
	v, found := this.cache.Get("Table:" + tableName)
	if found {
		return v.(*MetadataTable), found
	}
	return nil, false
}

func (this *MetadataCache) CacheSet(tableName string, table *MetadataTable) {
	this.cache.Set("Table:"+tableName, table, 10*time.Minute)

}
