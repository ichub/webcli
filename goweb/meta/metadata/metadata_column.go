package metadata

import (
	"encoding/json"
	"gitee.com/ichub/webcli/goconfig/base/basedto"
	"gitee.com/ichub/webcli/goconfig/base/meta/metadatacontext"
	"strings"
)

/*
@Title    文件名称: coulmns.go
@Description  描述: 元数据--表字段

@Author  作者: leijianming@163.com  时间(2024-01-31 22:38:21)
@Update  作者: leijianming@163.com  时间(2024-01-31 22:38:21)
*/
type MetadataColumn struct {
	basedto.BaseEntity
	TableName     string `json:"table_name",gorm:"column:table_name"`
	TableSchema   string `json:"table_schema",gorm:"column:table_schema"`
	ColumnName    string `json:"column_name",gorm:"column:column_name"`
	DataType      string `json:"data_type,"gorm:"column:data_type"`
	ColumnType    string `json:"column_type",gorm:"column:column_type"`
	ColumnKey     string `json:"column_key",gorm:"column:column_key"`
	CharMaxLen    string `json:"char_max_len",gorm:"column:char_max_len"`
	ColumnComment string `json:"column_comment",gorm:"column:column_comment"`
	ColumnDefault string `json:"column_default",gorm:"column:column_default"`
}

func NewMetadataColumns() *MetadataColumn {
	var cs = &MetadataColumn{}
	cs.InitProxy(cs)
	return cs
}

func (this *MetadataColumn) String() string {
	s, _ := json.Marshal(this)
	return string(s)
}
func (this *MetadataColumn) ToString() string {
	s, _ := json.MarshalIndent(this, "", "    ")
	return string(s)
}
func (this *MetadataColumn) IfString() bool {
	return this.FindGoType(this.DataType) == "string"
}

func (this *MetadataColumn) ReturnValue() (ReturnValue string) {
	ReturnValue = ""
	if this.IfInt() || this.IfNumeric() {
		ReturnValue = "return 0"
	} else if this.IfBool() || this.IfBitField() {
		ReturnValue = "return false"
	} else if this.IfString() {
		ReturnValue = `return ""`
	} else if this.IfTime() {
		ReturnValue = `return time.Time{}`
	} else if this.IfLocalTimeInt() {
		ReturnValue = `return basemodel.LocalTimeInt{}`
	} else if this.IfLocalTimeUTCInt() {
		ReturnValue = `return basemodel.LocalTimeUTCInt{}`
	} else if this.IfLocalDateInt() {
		ReturnValue = `return basemodel.LocalDateInt{}`
	} else if this.IfLocalTime() {
		ReturnValue = `return basemodel.LocalTime {}`
	} else if this.IfLocalDate() {
		ReturnValue = `return basemodel.LocalDate {}`
	}
	return
}

func (this *MetadataColumn) IfBool() bool {
	return this.FindGoType(this.DataType) == "bool"
}

func (this *MetadataColumn) IfBitField() bool {
	return strings.Contains(this.FindGoType(this.DataType), "BitField")
}

func (this *MetadataColumn) IfInt() bool {
	return strings.Contains(this.FindGoType(this.DataType), "int")
}
func (this *MetadataColumn) IfInt64() bool {
	return strings.Contains(this.FindGoType(this.DataType), "int64")
}
func (this *MetadataColumn) IfNumeric() bool {
	return this.IfInt() || strings.Contains(this.FindGoType(this.DataType), "decimal") || strings.Contains(this.FindGoType(this.DataType), "numeric") || strings.Contains(this.FindGoType(this.DataType), "float")

}

func (this *MetadataColumn) IfTime() bool {
	return strings.Contains(this.FindGoType(this.DataType), "time.Time")
}

func (this *MetadataColumn) IfLocalTimeInt() bool {
	return strings.Contains(this.FindGoType(this.DataType), "basemodel.LocalTimeInt")
}
func (this *MetadataColumn) IfLocalTimeUTCInt() bool {
	return strings.Contains(this.FindGoType(this.DataType), "basemodel.LocalTimeUTCInt")
}

func (this *MetadataColumn) IfLocalDateInt() bool {
	return strings.Contains(this.FindGoType(this.DataType), "basemodel.LocalDateInt")
}

func (this *MetadataColumn) IfLocalTime() bool {
	return strings.Contains(this.FindGoType(this.DataType), "basemodel.LocalTime")
}

func (this *MetadataColumn) IfLocalDate() bool {
	return strings.Contains(this.FindGoType(this.DataType), "basemodel.LocalDate")
}

func (this *MetadataColumn) IfDateTime() bool {
	return strings.Contains(this.FindGoType(this.DataType), "time")
}

func (this *MetadataColumn) IfDate() bool {
	return strings.Contains(this.FindGoType(this.DataType), "date")
}

func (this *MetadataColumn) FindGoType(fieldType string) (goType string) {

	return metadatacontext.MetadataContextInst.FindGoType(fieldType)
}
func (this *MetadataColumn) FindColEsType() (goType string) {

	return metadatacontext.MetadataContextInst.FindEsType(this.DataType)
}
func (this *MetadataColumn) FindColGoType() (goType string) {

	return metadatacontext.MetadataContextInst.FindGoType(this.DataType)
}
