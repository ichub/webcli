package metadatafactroy

import "gitee.com/ichub/webcli/goconfig/base/meta/ichubmetadata"

type ImetadataFactroy interface {
	FindMetadata(table string) *ichubmetadata.MetadataTable
	FindFields(table string, fields string) string
}
