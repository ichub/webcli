package metadatafactroy

import (
	"container/list"
	"gitee.com/ichub/webcli/goconfig/ichubconfig"
	"gitee.com/ichub/webcli/goweb/meta/metadata"
)

var Cfg = ichubconfig.NewConfig()

type DefineFactors struct {
	Columns *[]metadata.MetadataColumn
	Models  *list.List
	//mappings
}
