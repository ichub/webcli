package indexmetadata

import (
	"gitee.com/ichub/webcli/goconfig/base/baseutils/jsonutils"
	"gitee.com/ichub/webcli/goconfig/ichublog"
	"github.com/sirupsen/logrus"
	"testing"
)

func init() {
	ichublog.InitLogrus()
}

var mi = NewIndexMetadata()

// "ichub_sys_dept": {
// "mappings": {
// "properties": {
func Test0001_addmapping(t *testing.T) {
	mi.SetIndexName("ichub_sys_dept")
	mi.AddField("id", "int")
	mi.AddField("desc", "text")

	logrus.Info(jsonutils.ToJsonPretty(mi.ToMappings()))
}
