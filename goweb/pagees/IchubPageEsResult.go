package pagees

import (
	"encoding/json"
	"gitee.com/ichub/webcli/goconfig/base/baseconsts"
	"gitee.com/ichub/webcli/goweb/page"
	"github.com/olivere/elastic"
)

/*
@Title    文件名称: pageesresponse.go
@Description  描述:  es响应消息

@Author  作者: leijianming@163.com  时间(2024-02-22 22:38:21)
@Update  作者: leijianming@163.com  时间(2024-02-21 22:38:21)
*/
type IchubPageEsResult struct {
	page.IchubPageResult
	SearchResult *elastic.SearchResult `json:"search_result,omitempty"`
}

func NewIchubPageEsResult() *IchubPageEsResult {
	var result = &IchubPageEsResult{}
	result.InitProxy(result)
	return result
}
func (this *IchubPageEsResult) Hits2Sources(hits []*elastic.SearchHit) *[]json.RawMessage {
	var rawmsg = []json.RawMessage{}
	for _, _ = range hits {
		//rawmsg = append(rawmsg, v.Source.)
	}
	return &rawmsg
}

func (this *IchubPageEsResult) ValueOf(that *IchubPageEsRequest, SearchResult *elastic.SearchResult) {
	this.PageCurrent = that.PageCurrent
	this.PageCurrent = that.PageCurrent
	this.InitPage()
	this.Success()
	this.SearchResult = SearchResult
	this.Data = this.Hits2Sources(SearchResult.Hits.Hits)
	//this.Total = int(SearchResult.Hits.TotalHits.Value)
}

func (this *IchubPageEsResult) FailMsg(Msg string) *IchubPageEsResult {

	this.IchubPageResult.FailMsg(Msg)
	return this
}
func (this *IchubPageEsResult) InitPage() {
	if this.PageSize <= baseconsts.PAGE_SIZE_ZERO {

		this.PageSize = baseconsts.PAGE_SIZE_DEFAULT

	} else if this.PageSize > baseconsts.PAGE_SIZE_MAX {

		this.PageSize = baseconsts.PAGE_SIZE_MAX

	}
	if this.PageCurrent <= 0 {
		this.PageCurrent = baseconsts.PAGE_CURRENT
	}

}
