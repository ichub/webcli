package pagees

import (
	"gitee.com/ichub/webcli/goweb/pagebase"
	"github.com/olivere/elastic"
)

/*
@Title    文件名称: PageEsQueryService.go
@Description  描述: PageEsQueryService

@Author  作者: leijianming@163.com  时间(2024-02-22 22:38:21)
@Update  作者: leijianming@163.com  时间(2024-02-21 22:38:21)
*/
//https://blog.csdn.net/K346K346/article/details/120906440
//rangeQuery := elastic.NewRangeQuery("age").Gte(18).Lte(35)

type PageEsQueryService struct {
	pageReq *IchubPageEsRequest `json:"page_req"`
}

func NewEsQueryService(pageReq *IchubPageEsRequest) *PageEsQueryService {
	return &PageEsQueryService{pageReq: pageReq}
}
func (this *PageEsQueryService) BuildQuery() elastic.Query {
	var queries = []elastic.Query{}

	queries = append(queries, this.MatchQuery()...)
	//queries=append(queries,this.TermQuery()...)

	//	queries = append(queries, this.FuzzyQuery()...)
	queries = append(queries, this.TermsQuery()...)
	queries = append(queries, this.RangeQuery()...)
	if len(queries) == 0 {
		return nil
	}
	if len(queries) == 1 { //	searchService.Query(queries[0])
		return queries[0]
	}
	boolQuery := elastic.NewBoolQuery()
	boolQuery.Must(queries...)
	return boolQuery

}

//func (this *PageEsQueryService) Query(searchService *elastic.SearchService) {
//	var queries = []elastic.Query{}
//	var matchQuery = this.MatchQuery()
//	if matchQuery != nil {
//		//	searchService.Query(matchQuery)
//		//	queries = append(queries, matchQuery)
//	}
//
//	for _, q := range this.FuzzyQuery() {
//		queries = append(queries, q)
//	}
//
//	var termQuery = this.TermQuery()
//	if termQuery != nil {
//		queries = append(queries, termQuery)
//	}
//	var termsQuery = this.TermsQuery()
//	if termsQuery != nil {
//		//	searchService.Query(termsQuery)
//		queries = append(queries, termsQuery)
//	}
//
//	for _, q := range this.RangeQuery() {
//		queries = append(queries, q)
//	}
//
//	if len(queries) == 0 {
//		return
//	}
//	if len(queries) == 1 {
//		searchService.Query(queries[0])
//		return
//	}
//
//	boolQuery := elastic.NewBoolQuery()
//	boolQuery.Must(queries...)
//	searchService.Query(boolQuery)
//
//}

// match term
// keyword
func (this *PageEsQueryService) TermQuery() []elastic.Query {
	var qs = []elastic.Query{}

	for _, v := range this.pageReq.Fields {
		if v.OpType == this.pageReq.FindFieldSign(pagebase.EsTerm) {
			qs = append(qs, elastic.NewTermQuery(v.Field, v.Values[0].(string)))
		}

	}
	return qs

}
func (this *PageEsQueryService) TermsQuery() []elastic.Query {
	var qs = []elastic.Query{}
	for _, v := range this.pageReq.Fields {

		if v.OpType == this.pageReq.FindFieldSign(pagebase.In) {
			qs = append(qs, elastic.NewTermsQuery(v.Field, v.Values...))
		}
	}
	return qs

}
func (this *PageEsQueryService) FuzzyQuery() []elastic.Query {
	var queries = []elastic.Query{}
	for _, v := range this.pageReq.Fields {
		if v.OpType == this.pageReq.FindFieldSign(pagebase.EsFuzzy) {
			queries = append(queries, elastic.NewFuzzyQuery(v.Field, v.Values[0].(string)))
		}
		if v.OpType == this.pageReq.FindFieldSign(pagebase.Like) {
			queries = append(queries, elastic.NewFuzzyQuery(v.Field, v.Values[0].(string)))
		}
	}
	return queries

}

// text
func (this *PageEsQueryService) MatchQuery() []elastic.Query {
	var qs = []elastic.Query{}
	for _, v := range this.pageReq.Fields {
		if v.OpType == this.pageReq.FindFieldSign(pagebase.Eq) {
			qs = append(qs, elastic.NewMatchQuery(v.Field, v.Values[0].(string)))
		}
	}
	return qs

}
func (this *PageEsQueryService) RangeQuery() []elastic.Query {
	var quries = []elastic.Query{}
	for _, v := range this.pageReq.Fields {
		if v.OpType == this.pageReq.FindFieldSign(pagebase.Lt) {
			quries = append(quries, elastic.NewRangeQuery(v.Field).Lt(v.Values[0]))
		}
		if v.OpType == this.pageReq.FindFieldSign(pagebase.Le) {
			quries = append(quries, elastic.NewRangeQuery(v.Field).Lte(v.Values[0]))
		}
		if v.OpType == this.pageReq.FindFieldSign(pagebase.Gt) {
			quries = append(quries, elastic.NewRangeQuery(v.Field).Gt(v.Values[0]))
		}
		if v.OpType == this.pageReq.FindFieldSign(pagebase.Ge) {
			quries = append(quries, elastic.NewRangeQuery(v.Field).Gte(v.Values[0]))
		}

		if v.OpType == this.pageReq.FindFieldSign(pagebase.Between) {
			quries = append(quries, elastic.NewRangeQuery(v.Field).Gte(v.Values[0]).Lte(v.Values[1]))
		}
		if v.OpType == this.pageReq.FindFieldSign(pagebase.NotBetween) {
			quries = append(quries, elastic.NewRangeQuery(v.Field).Gt(v.Values[1]))
			quries = append(quries, elastic.NewRangeQuery(v.Field).Gt(v.Values[0]))

		}
	}
	return quries

}
