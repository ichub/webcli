package webserver

import (
	"gitee.com/ichub/webcli/goweb/webserver/middleware"
	"github.com/gin-gonic/gin"
	"net/http"
)

/*
@Title    文件名称: web_routers.go
@Description  描述: 规则引擎路由服务

@Author  作者: leijianming@163.com  时间(2024-02-18 22:38:21)
@Update  作者: leijianming@163.com  时间(2024-02-18 22:38:21)
*/
type WebRouters struct {
	WebHandlers  *[]WebHandler
	FuncRegister func() *[]WebHandler
}

func (web *WebRouters) BuildRouter() *gin.Engine {
	web.WebHandlers = web.FuncRegister()
	return web.NewRouter()
}

func (handlers *WebRouters) NewRouter() *gin.Engine {

	//gin.SetMode(gin.ReleaseMode)
	router := gin.Default()
	router.Use(middleware.Cors())

	handlers.AddRouter(router)
	//router.GET("/", func(c *gin.Context) {
	//	c.IndentedJSON(http.StatusOK, (&basedto.IchubResult{}).SuccessData("Hello World"))
	//})
	//router.GET("/help", ruleservice.Help)
	//router.GET("/alladdr", ruleservice.AllAddr)
	//router.POST("/ichubengine", ruleservice.DoIchubEngineMsg)
	//router.POST("/ichubengineBatch", ruleservice.DoIchubEngineMsgs)

	//router.LoadHTMLGlob("templates/*")
	//loadTemplte(router)
	return router
}

// MethodGet     = "GET"
// MethodHead    = "HEAD"
// MethodPost    = "POST"
// MethodPut     = "PUT"
// MethodPatch   = "PATCH" // RFC 5789
// MethodDelete  = "DELETE"
func (handlers *WebRouters) AddRouter(router *gin.Engine) {
	for _, h := range *handlers.WebHandlers {
		switch h.Method {
		case http.MethodGet:
			router.GET(h.RelativePath, h.Handler)

		case http.MethodPost:
			router.POST(h.RelativePath, h.Handler)
		case http.MethodPut:
			router.PUT(h.RelativePath, h.Handler)

		case http.MethodDelete:
			router.DELETE(h.RelativePath, h.Handler)
		case http.MethodPatch:
			router.PATCH(h.RelativePath, h.Handler)
		case http.MethodHead:
			router.HEAD(h.RelativePath, h.Handler)

		}
	}
}
func (handlers *WebRouters) loadTemplte(r *gin.Engine) {
	r.LoadHTMLGlob("templates/*")
	//r.LoadHTMLFiles("templates/posts/index.html", "templates/users/index.html")

	// HTML文件中引用了静态文件时
	//r.Static("/html中使用的起始路径", "/文件真实路径"
}
