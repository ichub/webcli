package webserver

import "github.com/gin-gonic/gin"

type HttpMethod string
type WebHandler struct {
	Method       HttpMethod
	RelativePath string
	Handler      gin.HandlerFunc
}
