package pagedb

import (
	"gitee.com/ichub/webcli/goconfig/base/meta/ichubmetadata"
	"gitee.com/ichub/webcli/goconfig/ichublog"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"strings"
	"testing"
	"time"
)

/*
	@Title    文件名称: generalquery.go
	@Description  描述:  通用查询

	@Author  作者: leijianming@163.com  时间(2024-02-22 22:38:21)
	@Update  作者: leijianming@163.com  时间(2024-02-21 22:38:21)
*/

//http://www.forenose.com/column/content/427118110.html
//领域事件建模

func init() {
	ichublog.InitLogrus()
}
func Test001_SelectEmployeeGeneral(t *testing.T) {

	var dbreq = NewPageDbRequest(1)
	dbreq.TableName = "department"
	var result = dbreq.GeneralQuery()

	assert.Equal(t, 200, result.Code)
	ichublog.Log(result.ToPrettyString())

}
func Test002_SelectEmployeeGeneral(t *testing.T) {

	var dbreq = NewIchubPageDbRequest()
	dbreq.PageSize = 2
	dbreq.TableName = "employee"
	//	dbreq.FieldsName = "id,name,departmentId"

	var result = dbreq.GeneralQuery()
	ichublog.Log(result.ToPrettyString())

	logrus.Info(dbreq.ToPrettyString())

}

// IchubPageDbRequest
func Test003_SelectRuleDbRequest(t *testing.T) {

	var dbRequest = NewIchubPageDbRequest()
	dbRequest.PageSize = 2
	dbRequest.TableName = "rules"
	dbRequest.FieldsName = strings.Join(ichubmetadata.DefaultFields, ",")
	//"rule_id,rule_key,name,created_at"
	dbRequest.Ge("rule_id", 3)
	dbRequest.OrderBy("rule_id", "asc")

	var result = dbRequest.GeneralQuery()
	ichublog.Log(result.ToPrettyString())
	assert.Equal(t, 200, result.Code)

}

func Test0004_SelectEmpRequest(t *testing.T) {

	var dbreq = NewIchubPageDbRequest()
	dbreq.PageSize = 2
	dbreq.TableName = "employee"
	dbreq.Lt("birthday", time.Now())

	var result = dbreq.GeneralQuery()
	logrus.Info(result.ToPrettyString())
	ichublog.Log(result.ToPrettyString())
	assert.Equal(t, 200, result.Code)
}

func Test0005_QueryDepart(t *testing.T) {
	var pageRequest = NewIchubPageDbRequest()
	pageRequest.PageSize = 4
	pageRequest.OrderBy("order_num", "asc") //	pageRequest.InFields("ruleId", []string{"1", "2221"})
	pageRequest.OrderBy("DeptId", "asc")    //	pageRequest.InFields("ruleId", []string{"1", "2221"})
	pageRequest.Ge("DeptId", 101)
	pageRequest.TableName = "sys_dept"
	pageRequest.FieldsName = "dept_name,order_num,deptId,createTime"
	//	pageRequest.FieldsName = "dept_id,create_time"
	var result = pageRequest.GeneralQuery()
	ichublog.Log(result.ToPrettyString())
	assert.Equal(t, 200, result.Code)
}
