package pagedb

import (
	"gitee.com/ichub/webcli/goconfig/base/basedto"
	"gitee.com/ichub/webcli/goconfig/base/baseutils/jsonutils"
	"gitee.com/ichub/webcli/goconfig/ichublog"
	pagemodel2 "gitee.com/ichub/webcli/goweb/pagedb/pagemodel"
	"github.com/sirupsen/logrus"
	"testing"
	"time"
)

/*
	@Title    文件名称: aggtest.go
	@Description  描述:  聚合根构建

	@Author  作者: leijianming@163.com  时间(2024-02-22 22:38:21)
	@Update  作者: leijianming@163.com  时间(2024-02-21 22:38:21)
*/

//http://www.forenose.com/column/content/427118110.html
//领域事件建模

func init() {
	ichublog.InitLogrus()
}

type DeptDto map[string]interface{}
type EmpDtos []map[string]interface{}

type DeptEmp struct {
	Dept *DeptDto `json:"dept,omitempty"`
	Emp  EmpDtos  `json:"emp,omitempty"`
}

func NewDeptEmp() *DeptEmp {
	return &DeptEmp{}
}

func LoadDpt(id int64) *DeptDto {
	var pageDbRequest = NewPageDbRequest(1)
	pageDbRequest.TableName = "department"
	pageDbRequest.Eq("id", id)
	//	pageDbRequest.FieldsName = "id,name,code"
	var result = pageDbRequest.GeneralQuery()
	var dto DeptDto = result.Data.([]map[string]interface{})[0]
	return &dto
}
func LoadEmp(department_id int64) EmpDtos {

	var pageDbRequest = NewPageDbRequest(1000)
	pageDbRequest.TableName = "employee"
	pageDbRequest.Eq("department_id", department_id)
	//pageDbRequest.FieldsName = "begin_date,id,code,department_id,name,gender"
	var result = pageDbRequest.GeneralQuery()
	logrus.Info("LoadEmp=", result.ToPrettyString())
	var emps = result.Data.([]map[string]interface{})
	var res = jsonutils.ToJsonPretty(emps)
	var empdtos EmpDtos
	jsonutils.FromJson(res, &empdtos)
	return empdtos
}
func Build(id int64) *DeptEmp {
	var dept = LoadDpt(id)
	var dptemp = NewDeptEmp()
	if dept != nil {
		dptemp.Dept = dept
		dptemp.Emp = LoadEmp(id)

	}
	return dptemp
}

func Test001_SelDeptEmpMap(t *testing.T) {
	logrus.Info("创建聚合根 starting...")
	var dptemp = Build(1)
	logrus.Info(jsonutils.ToJsonPretty(dptemp))
	logrus.Info("\r\n创建聚合根end")

	ichublog.Log("\r\n部门人员聚合根=\r\n", jsonutils.ToJsonPretty(dptemp))

}

type DptAgg struct {
	Dept pagemodel2.Department `json:"dept"`
	Emp  []pagemodel2.Employee `json:"emp"`

	basedto.BaseEntity `json:"-"`
}

func NewDptAgg() *DptAgg {
	var agg = &DptAgg{
		Emp: []pagemodel2.Employee{},
	}
	agg.InitProxy(agg)
	return agg
}

//func (this *DptAgg) ValueOf(from *DeptEmp) {
//
//	//	var fromstr = jsonutils.ToJsonPretty(from)
//	//jsonutils.FromJson(fromstr, this)
//	this.DecodeFrom(from) // 	mapstructure.Decode(from, this)
//}

func Test002_SelDeptEmp(t *testing.T) {
	logrus.Info("创建聚合根 starting...")
	var dptemp = Build(1)
	logrus.Info("sel=", jsonutils.ToJsonPretty(dptemp))
	var agg = NewDptAgg()
	logrus.Info("\r\n创建聚合根end")
	agg.ValueFrom(dptemp)

	ichublog.Log("\r\n部门人员聚合根agg=\r\n", agg)
	logrus.Info("agg=", agg.ToPrettyString(), time.Now())

}
