package godi

import (
	"errors"
	"gitee.com/ichub/webcli/goconfig/base/baseiface"
	"gitee.com/ichub/webcli/goconfig/base/baseutils/fileutils"
	"gitee.com/ichub/webcli/goconfig/base/baseutils/jsonutils"
	"gitee.com/ichub/webcli/goconfig/ichublog"
	"gitee.com/ichub/webcli/godi/didto"
	"gitee.com/ichub/webcli/godi/difactroy"

	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/suite"
	"runtime"
	"testing"
)

type TestDiFactoySuite struct {
	suite.Suite
	DiFactroy *difactroy.DiFactroy
	rootdir   string
}

func (this *TestDiFactoySuite) SetupTest() {
	logrus.Info("difactry...")
	ichublog.InitLogrus()
	this.rootdir = fileutils.FindRootDir()
	this.DiFactroy = difactroy.NewDiFactroy()

	var i baseiface.ISingleton = nil
	logrus.Info(i)

}

func TestDiFactoySuites(t *testing.T) {
	suite.Run(t, new(TestDiFactoySuite))
}
func CurrentFile() string {
	_, file, _, ok := runtime.Caller(1)
	if !ok {
		panic(errors.New("Can not get current file info"))
	}
	return file
}

// 指定一个文件生成
func (this *TestDiFactoySuite) Test001_MakeDiFile() {

	this.DiFactroy.MakeDiFile("./dimodel/multi/multi_entity.go")
	logrus.Info(CurrentFile())

}

// 指定结构体structName生成注册与注入代码
func (this *TestDiFactoySuite) Test002_MakeDiStru() {

	this.DiFactroy.Rootdir = fileutils.FindRootDir()
	this.DiFactroy.MakeDiStru("MultiEntity")

}

// 生成所有, 继承baseentity的结果: 已经有不再生成！
func (this *TestDiFactoySuite) Test003_MakeDiAll() {

	this.DiFactroy.MakeDiAll()

}

// 生成所有 继承baseentity的结果，强制执行
func (this *TestDiFactoySuite) Test004_MakeDiForce() {

	this.DiFactroy.MakeDiAllForce(true)

}

// 生成一个结构体的注册文件
func (this *TestDiFactoySuite) Test006_MakeDiDto() {
	var dto = didto.FindBeanDiDto()
	dto.PkgName = "didto"
	dto.StructName = "DiDto"
	this.DiFactroy.MakeDi(dto)
}

// 查找所有GO文件
func (this *TestDiFactoySuite) Test007_FindAllFiles() {
	this.DiFactroy.FindGoFiles()

	logrus.Info("basedi=", jsonutils.ToJsonPretty(this.DiFactroy))
}

// AST分析所有文件
func (this *TestDiFactoySuite) Test008_ParseAll() {

	this.DiFactroy.ParseAll()
	var _, found = this.DiFactroy.FindSome("SingleEntity")
	if found {

	} else {
		logrus.Error("\r\n!found ", found)
	}
}
