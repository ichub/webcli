package didto

import (
	"gitee.com/webcli/goconfig/ichublog"
	"gitee.com/webcli/godi/codefactroy"
	"github.com/stretchr/testify/suite"
	"testing"
)

type TestDiDtoSuite struct {
	suite.Suite
}

func TestDiDtoSuites(t *testing.T) {
	suite.Run(t, new(TestDiDtoSuite))
}

func (this *TestDiDtoSuite) SetupTest() {

	ichublog.InitLogrus()
}

func (this *TestDiDtoSuite) Test001_MakeDi() {
	var dto = NewDiDto()
	dto.PkgName = "didto"
	dto.StructName = "DiDto"
	codefactroy.MakeDi(dto)

}
func (this *TestDiDtoSuite) Test002_FindDi() {
	var dto = FindBeanDiDto()
	dto.PkgName = "didto"
	dto.StructName = "DiDto"
	codefactroy.MakeDi(dto)

}
