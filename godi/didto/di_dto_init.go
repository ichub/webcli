package didto

import (
	"gitee.com/webcli/goconfig/base/baseiface"
	"gitee.com/webcli/goconfig/basedi"
	"github.com/sirupsen/logrus"
)

/*
	@Title   文件名称: di_dto_init.go
	@Desp    描述:    自动注册注入
    @Company 公司:    kunlong@sz.com

	@Author  作者:    LEIJMDAS@163.COM  时间: 2024-05-05 13:32:16
	@Update  作者:    LEIJMDAS@163.COM  时间: 2024-05-05 13:32:16
*/

const singleNameDiDto = "didto.DiDto"

// init register load
func init() {
	registerBeanDiDto()
}

// register DiDto
func registerBeanDiDto() {
	basedi.RegisterLoadBean(singleNameDiDto, LoadDiDto)
}

func FindBeanDiDto() *DiDto {
	return basedi.FindBean(singleNameDiDto).(*DiDto)
}

func LoadDiDto() baseiface.ISingleton {
	var s = NewDiDto()
	InjectDiDto(s)
	return s

}

func InjectDiDto(s *DiDto) {

	logrus.Debug("inject")
}
