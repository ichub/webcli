package didto

import (
	"gitee.com/webcli/goconfig/base/baseiface"
	"gitee.com/webcli/goconfig/basedi"
	"github.com/sirupsen/logrus"
)

/*
	@Title   文件名称: di_inject_init.go
	@Desp    描述:    自动注册注入
    @Company 公司:    kunlong@sz.com

	@Author  作者:    LEIJMDAS@163.COM  时间: 2024-05-05 13:32:16
	@Update  作者:    LEIJMDAS@163.COM  时间: 2024-05-05 13:32:16
*/

const singleNameDiInject = "didto.DiInject"

// init register load
func init() {
	registerBeanDiInject()
}

// register DiInject
func registerBeanDiInject() {
	basedi.RegisterLoadBean(singleNameDiInject, LoadDiInject)
}

func FindBeanDiInject() *DiInject {
	return basedi.FindBean(singleNameDiInject).(*DiInject)
}

func LoadDiInject() baseiface.ISingleton {
	var s = NewDiInject()
	InjectDiInject(s)
	return s

}

func InjectDiInject(s *DiInject) {

	logrus.Debug("inject")
}
