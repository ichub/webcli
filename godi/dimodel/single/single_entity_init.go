package single

import (
	"gitee.com/webcli/goconfig/base/baseiface"
	"gitee.com/webcli/goconfig/basedi"
	"gitee.com/webcli/godi/dimodel/multi"
	"github.com/sirupsen/logrus"
)

/*
	@Title   文件名称: single_entity_init.go
	@Desp    描述:    自动注册注入
    @Company 公司:    kunlong@sz.com

	@Author  作者:    LEIJMDAS@163.COM  时间: 2024-05-05 13:32:16
	@Update  作者:    LEIJMDAS@163.COM  时间: 2024-05-05 13:32:16
*/

const singleNameSingleEntity = "single.SingleEntity"

// init register load
func init() {
	registerBeanSingleEntity()
}

// register SingleEntity
func registerBeanSingleEntity() {
	basedi.RegisterLoadBean(singleNameSingleEntity, LoadSingleEntity)
}

func FindBeanSingleEntity() *SingleEntity {
	return basedi.FindBean(singleNameSingleEntity).(*SingleEntity)
}

func LoadSingleEntity() baseiface.ISingleton {
	var s = NewSingleEntity()
	InjectSingleEntity(s)
	return s

}

func InjectSingleEntity(s *SingleEntity) {

	// 实例注册函数
	s.Bind()
	// 实例查找函数
	if find := s.FindBinding("MultiBindNew"); find != nil {
		logrus.Debug("find binded LoadBeanMultiEntity!")
		// 类型断言执行load
		s.MultiBindNew = find().(*multi.MultiEntity)
	} else {
		// 未找到绑定，实例化
		s.MultiBindNew = &multi.MultiEntity{}
		logrus.Error("no binding LoadBeanMultiEntity!")
	}

	// 简单实例化
	s.MultiSingle = &multi.MultiEntity{}

	// 自动实例化
	s.MultiAuto = multi.FindBeanMultiEntity()

	// 实例注册函数
	s.Bind()
	// 实例查找函数
	if find := s.FindBinding("MultiBind"); find != nil {
		logrus.Debug("find binded LoadBeanMultiEntity!")
		// 类型断言执行load
		s.MultiBind = find().(*multi.MultiEntity)
	} else {
		// 未找到绑定，实例化
		s.MultiBind = &multi.MultiEntity{}
		logrus.Error("no binding LoadBeanMultiEntity!")
	}

	// 实例化默认New
	s.MultiNew = multi.NewMultiEntity()
	logrus.Debug("inject")
}
