package single

import (
	"gitee.com/webcli/goconfig/base/baseiface"
	"gitee.com/webcli/goconfig/basedi"
	"gitee.com/webcli/godi/didto"
	"gitee.com/webcli/godi/difactroy"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"testing"
)

var DiFactroy = difactroy.FindBeanDiFactroy()

// 注入的三种方式： auto/ new / bind
func Test001_FindSingleAutoInject(t *testing.T) {

	var singleEntity = FindBeanSingleEntity()
	logrus.Info(singleEntity)

}

// find single is same
func Test002_FindSingle(t *testing.T) {

	var singleEntity = FindBeanSingleEntity()

	singleEntity.Name = "aaa"

	logrus.Info(singleEntity.ToPrettyString())
	var sa = FindBeanSingleEntity()
	var sb = FindBeanSingleEntity()
	sb.Name = "bbb"
	logrus.Info(singleEntity, sa, sb)
	assert.Equal(t, sa.Name == singleEntity.Name, true)
	assert.Equal(t, sb.Name == singleEntity.Name, true)

}

func Test003_create(t *testing.T) {
	var single = NewSingleEntity()
	logrus.Info(single.ToPrettyString())
	single.Check()
	var s1 baseiface.ISingleton = single
	s1.(*SingleEntity).Id = 111
	logrus.Info("is=:", s1, s1.Single())
	logrus.Info("s=", single, single.Single())

}

// find single is same
func Test005_FindSingle(t *testing.T) {

	var bean1 = FindBeanSingleEntity()
	bean1.Name = "111"
	logrus.Info("proxy1=", bean1, bean1.Single())

	var bean2 = basedi.FindBean("single")
	var bean3 = basedi.FindBean("single")
	bean2.(*SingleEntity).Name = "1111222"
	logrus.Info("proxy1=", bean1, bean1.Single())
	logrus.Info("proxy2=", bean2, bean2.Single(), bean3)

}

func Test006_makedi(t *testing.T) {

	var dto = didto.FindBeanDiDto() //NewDiDto()
	dto.PkgName = "single"
	dto.StructName = "SingleEntity"
	dto.ForceBuild = true
	DiFactroy.MakeDi(dto)
}
