package multi

import "gitee.com/webcli/goconfig/base/basedto"

type MyEntity struct {
	basedto.BaseEntity
}

func newMy() *MyEntity {
	return &MyEntity{}
}
