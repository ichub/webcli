package multi

import (
	"gitee.com/webcli/godi/didto"
	"gitee.com/webcli/godi/difactroy"
	"github.com/gookit/goutil/testutil/assert"
	"github.com/sirupsen/logrus"
	"testing"
)

var DiFactroy = difactroy.NewDiFactroy()

// find is not support multi entity
func Test001_FindMulti(t *testing.T) { //var bean1 = FindBeanMultiEntity()

	var bean2 = FindBeanMultiEntity()
	var bean3 = FindBeanMultiEntity()
	bean2.Name = "1111222"
	logrus.Info(bean2, bean3)
	assert.Equal(t, bean3.Name != bean2.Name, true)

}
func Test002_makedi(t *testing.T) {

	var dto = didto.NewDiDto()
	dto.PkgName = "multi"
	dto.StructName = "MultiEntity"
	dto.ForceBuild = true
	DiFactroy.MakeDi(dto)
}
