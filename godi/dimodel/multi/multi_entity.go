package multi

import (
	"gitee.com/webcli/goconfig/base/basedto"
	"gitee.com/webcli/goconfig/base/baseiface"
	"gitee.com/webcli/goconfig/ichublog"
)

type MultiEntity struct {
	basedto.BaseEntity

	Id   int
	Name string

	myeentity *MyEntity `godi:"auto"`
}

func NewMultiEntity() *MultiEntity {
	var s = &MultiEntity{
		Name: "multi",
	}
	s.InitProxy(s)
	return s
}

func (MultiEntity) Autoload() bool {
	return true
}
func (MultiEntity) AutoInject() bool {
	return true
}

func (*MultiEntity) Check() {
	ichublog.InitLogrus()

	var proxy baseiface.IbaseProxy = basedto.NewIchubResult()
	var single, oksingle = proxy.(baseiface.ISingleton)

	ichublog.Log("single=", single, oksingle, single.Single())
}
