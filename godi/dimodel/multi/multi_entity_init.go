package multi

import (
	"gitee.com/webcli/goconfig/base/baseiface"
	"gitee.com/webcli/goconfig/basedi"
	"github.com/sirupsen/logrus"
)

/*
	@Title   文件名称: multi_entity_init.go
	@Desp    描述:    自动注册注入
    @Company 公司:    kunlong@sz.com

	@Author  作者:    LEIJMDAS@163.COM  时间: 2024-05-05 13:32:16
	@Update  作者:    LEIJMDAS@163.COM  时间: 2024-05-05 13:32:16
*/

const singleNameMultiEntity = "multi.MultiEntity"

// init register load
func init() {
	registerBeanMultiEntity()
}

// register MultiEntity
func registerBeanMultiEntity() {
	basedi.RegisterLoadBean(singleNameMultiEntity, LoadMultiEntity)
}

func FindBeanMultiEntity() *MultiEntity {
	return basedi.FindBean(singleNameMultiEntity).(*MultiEntity)
}

func LoadMultiEntity() baseiface.ISingleton {
	var s = NewMultiEntity()
	InjectMultiEntity(s)
	return s

}

func InjectMultiEntity(s *MultiEntity) {

	// 自动实例化
	s.myeentity = FindBeanMyEntity()
	logrus.Debug("inject")
}
