package multi

import (
	"gitee.com/webcli/goconfig/base/baseiface"
	"gitee.com/webcli/goconfig/basedi"
	"github.com/sirupsen/logrus"
)

/*
	@Title   文件名称: my_entity_init.go
	@Desp    描述:    自动注册注入
    @Company 公司:    kunlong@sz.com

	@Author  作者:    LEIJMDAS@163.COM  时间: 2024-05-05 13:32:16
	@Update  作者:    LEIJMDAS@163.COM  时间: 2024-05-05 13:32:16
*/

const singleNameMyEntity = "multi.MyEntity"

// init register load
func init() {
	registerBeanMyEntity()
}

// register MyEntity
func registerBeanMyEntity() {
	basedi.RegisterLoadBean(singleNameMyEntity, LoadMyEntity)
}

func FindBeanMyEntity() *MyEntity {
	return basedi.FindBean(singleNameMyEntity).(*MyEntity)
}

func LoadMyEntity() baseiface.ISingleton {
	var s = newMy()
	InjectMyEntity(s)
	return s

}

func InjectMyEntity(s *MyEntity) {

	logrus.Debug("inject")
}
