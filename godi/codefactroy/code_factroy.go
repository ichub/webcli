package codefactroy

import (
	"github.com/sirupsen/logrus"
	"path/filepath"
	"strings"
	"time"
)

const (
	VAR_KEY_FileName = "FileName"
	VAR_KEY_Author   = "Author"
	VAR_KEY_Corp     = "Corp"
	VAR_KEY_Now      = "Now"
)
const tEMPLATE_CODE_FILE = "config/godi/template/struct_init.template"

func MakeDi(dto *didto.DiDto) error {
	var cf = FindBeanCodeFactroy()
	cf.DiDto = dto
	cf.MakeToParam()
	return cf.WriteStructFile()

}

type CodeFactroy struct {
	basedto.BaseEntity
	rootdir string

	*didto.DiDto

	ExistNewStruct bool
	params         map[string]string
	cfg            *ichubconfig.IchubConfig
}

func NewCodeFactroy() *CodeFactroy {
	var cf = &CodeFactroy{
		rootdir:        fileutils.FindRootDir(),
		params:         make(map[string]string),
		DiDto:          didto.NewDiDto(),
		ExistNewStruct: true,
		cfg:            ichubconfig.NewConfig(),
	}
	cf.OutPath = "./"
	cf.InitProxy(cf)
	return cf
}

func (cf *CodeFactroy) Path() string {
	return cf.OutPath
}

func (cf *CodeFactroy) SetPath(outpath string) {
	cf.OutPath = outpath
}

/*
@Title    文件名称: {{.FileName}}.go
@Description  描述: 代码工厂服务

@Author  作者: {{.Author}}  时间({{.Now}})
@Update  作者: {{.Author}}  时间({{.Now}})
*/
func (cf *CodeFactroy) MakeToParam() {
	if len(cf.NewFuncName) == 0 {
		cf.NewFuncName = "New" + cf.StructName
	}
	cf.params["SinglePKG"] = cf.PkgName
	cf.params["SingleStruct"] = cf.StructName
	cf.params["SinglePkgStruct"] = cf.PkgName + "." + cf.StructName
	cf.params["NewSingle"] = "&" + cf.StructName + "{}"
	if cf.ExistNewStruct {
		cf.params["NewSingle"] = cf.NewFuncName + "()"
	}
	cf.params["ToInjectCode"] = cf.ToInjectCode()
	cf.params["ImportLibs"] = cf.ToInjectImportLib()

	cf.params[VAR_KEY_FileName] = stringutils.Camel2Case(cf.StructName) + "_init"
	/*
	  software: kunlong@sz.com 	/ author: leijmdas@163.com /  corp: huawei.akunlong.top
	*/
	cf.cfg.Read()
	if cf.cfg.ReadVar("software.author") == nil {
		cf.params[VAR_KEY_Author] = "LEIJMDAS@163.COM"
		cf.params[VAR_KEY_Corp] = "kunlong@sz.com"
	} else {
		cf.params[VAR_KEY_Author] = cf.cfg.ReadVar("software.author").(string)
		cf.params[VAR_KEY_Corp] = cf.cfg.ReadVar("software.corp").(string)
	}
	cf.params[VAR_KEY_Now] = time.Now().Format(baseconsts.FormatDateTime)

}

func (cf *CodeFactroy) ReadTemplate() (string, error) {

	return fileutil.ReadFileToString(cf.rootdir + "/" + tEMPLATE_CODE_FILE)

}

func (cf *CodeFactroy) ParseParams() (string, error) {

	var content, err = cf.ReadTemplate()
	if err != nil {
		logrus.Error(err)
		return "", err
	}
	for k, v := range cf.params {
		content = strings.ReplaceAll(content, "{{."+k+"}}", v)
	}
	return content, err

}

func (cf *CodeFactroy) WriteFile(filename string) error {

	var params, _ = cf.ParseParams()
	path, _ := filepath.Abs(cf.OutPath)
	cf.OutFile = path + "/" + filename
	if !cf.ForceBuild && fileutils.CheckFileExist(cf.OutFile) {
		return nil
	}
	cf.ForceBuild = true
	ichublog.Log(cf.ToPrettyString())
	return fileutil.WriteBytesToFile(cf.OutFile, []byte(params))

}

func (cf *CodeFactroy) StructFileName() string {
	return stringutils.Camel2Case(cf.StructName) + "_init.go"
}

func (cf *CodeFactroy) WriteStructFile() error {

	return cf.WriteFile(cf.StructFileName())

}
