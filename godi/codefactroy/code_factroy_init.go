package codefactroy

import (
	"github.com/sirupsen/logrus"
)

/*
	@Title   文件名称: code_factroy_init.go
	@Desp    描述:    自动注册注入
    @Company 公司:    kunlong@sz.com

	@Author  作者:    LEIJMDAS@163.COM  时间: 2024-05-05 13:32:16
	@Update  作者:    LEIJMDAS@163.COM  时间: 2024-05-05 13:32:16
*/

const singleNameCodeFactroy = "codefactroy.CodeFactroy"

// init register load
func init() {
	registerBeanCodeFactroy()
}

// register CodeFactroy
func registerBeanCodeFactroy() {
	basedi.RegisterLoadBean(singleNameCodeFactroy, LoadCodeFactroy)
}

func FindBeanCodeFactroy() *CodeFactroy {
	return basedi.FindBean(singleNameCodeFactroy).(*CodeFactroy)
}

func LoadCodeFactroy() baseiface.ISingleton {
	var s = NewCodeFactroy()
	InjectCodeFactroy(s)
	return s

}

func InjectCodeFactroy(s *CodeFactroy) {

	logrus.Debug("inject")
}
