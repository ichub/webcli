package codefactroy

import (
	"gitee.com/webcli/goconfig/base/fileutils"
	"gitee.com/webcli/goconfig/ichublog"
	"gitee.com/webcli/godi/didto"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/suite"
	"testing"
)

type TestCodeFactoySuite struct {
	suite.Suite
}

func TestCodeFactoySuites(t *testing.T) {
	suite.Run(t, new(TestCodeFactoySuite))
}

func (this *TestCodeFactoySuite) SetupTest() {
	fileutils.FindRootDir()
	ichublog.InitLogrus()
}

func (this *TestCodeFactoySuite) Test001_MakeDi() {
	var dto = didto.FindBeanDiDto()
	dto.PkgName = "codefactroy"
	dto.StructName = "CodeFactroy"
	dto.ForceBuild = true
	MakeDi(dto)
	logrus.Info(dto.ToPrettyString())

}

func (this *TestCodeFactoySuite) Test002_MakeDiFile() {
	var dto = didto.FindBeanDiDto() //NewDiDto()
	dto.PkgName = "codefactroy"
	dto.StructName = "CodeFactroy"
	MakeDi(dto)

	logrus.Info(dto.ToPrettyString())
	dto = didto.FindBeanDiDto() //NewDiDto()
	dto.PkgName = "codefactroy1111"
	logrus.Info(dto.ToPrettyString())
}
