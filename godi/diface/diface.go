package diface

import "gitee.com/webcli/godi/difactroy"

type DiFactroy interface {
	CodeFace
	FindGoFiles() error
	FindBasePkg() string

	Parse(file string) *difactroy.FileInfoDto
	ParseAll()

	MakeDiOne(structInfo *difactroy.StructInfo) bool
	MakeDiAll() int
}
