package diface

import "gitee.com/webcli/godi/didto"

type CodeFace interface {
	MakeDi(dto *didto.DiDto) error
}
