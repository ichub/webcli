package diutils

import (
	"gitee.com/webcli/goconfig/base/basedto"
	"gitee.com/webcli/goconfig/base/fileutils"
	"gitee.com/webcli/goconfig/ichubconfig"
)

func FindKey(key string) bool {
	return FindBeanDiUtils().FindKey(key)
}

const ini_di_key = "godi"
const ini_di_gen = "generate"
const di_define_file = "/config/godi/di_define.ini"

type DiUtils struct {
	basedto.BaseEntitySingle
	rootdir string
	ini     *ichubconfig.IchubConfigIni `json:"-"`
	//key struct ->bool
	Items map[string]string
	Rerun map[string]string
}

func NewDiUtils() *DiUtils {
	var u = &DiUtils{
		rootdir: fileutils.FindRootDir(),
		ini:     ichubconfig.NewIchubConfigIni(),
	}
	u.init()
	return u
}

func (u *DiUtils) init() {
	u.LoadConfig()
}

func (this *DiUtils) FindKey(key string) bool {
	b, ok := this.Items[key]
	return b == "true" || ok
}

/*
#auto init generate
[generate]
rerun = true
#重新生成

[godi]
IchubConfig = true
SingleEntity = true
*/
func (this *DiUtils) LoadConfig() {
	this.ini.Load(this.rootdir + "/" + di_define_file)
	this.Items = this.ini.FindMap(ini_di_key)
	this.Rerun = this.ini.FindMap(ini_di_gen)
}
