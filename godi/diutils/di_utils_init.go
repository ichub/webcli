package diutils

import (
	"gitee.com/webcli/goconfig/base/baseiface"
	"gitee.com/webcli/goconfig/basedi"
	"github.com/sirupsen/logrus"
)

/*
	@Title   文件名称: di_utils_init.go
	@Desp    描述:    自动注册注入
    @Company 公司:    kunlong@sz.com

	@Author  作者:    LEIJMDAS@163.COM  时间: 2024-05-05 13:32:16
	@Update  作者:    LEIJMDAS@163.COM  时间: 2024-05-05 13:32:16
*/

const singleNameDiUtils = "diutils.DiUtils"

// init register load
func init() {
	registerBeanDiUtils()
}

// register DiUtils
func registerBeanDiUtils() {
	basedi.RegisterLoadBean(singleNameDiUtils, LoadDiUtils)
}

func FindBeanDiUtils() *DiUtils {
	return basedi.FindBean(singleNameDiUtils).(*DiUtils)
}

func LoadDiUtils() baseiface.ISingleton {
	var s = NewDiUtils()
	InjectDiUtils(s)
	return s

}

func InjectDiUtils(s *DiUtils) {

	logrus.Debug("inject")
}
