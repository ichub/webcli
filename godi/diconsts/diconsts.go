package diconsts

const (
	BaseEntity       = "BaseEntity"
	BaseEntitySingle = "BaseEntitySingle"

	INJECT_ENTITY_NONE   = iota
	INJECT_ENTITY_SINGLE = iota
	INJECT_ENTITY_AUTO
	INJECT_ENTITY_NEW
	INJECT_ENTITY_BIND
)
