package diconsts

const (
	TAG_GODI_NAME = "godi"

	TAG_VALUE_TYPE_NONE   = "none"
	TAG_VALUE_TYPE_SINGLE = "single"
	TAG_VALUE_TYPE_AUTO   = "auto"
	TAG_VALUE_TYPE_NEW    = "new"
	TAG_VALUE_TYPE_BIND   = "bind"
)
