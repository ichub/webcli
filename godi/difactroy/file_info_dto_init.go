package difactroy

import (
	"gitee.com/webcli/goconfig/base/baseiface"
	"gitee.com/webcli/goconfig/basedi"
	"github.com/sirupsen/logrus"
)

/*
	@Title   文件名称: file_info_dto_init.go
	@Desp    描述:    自动注册注入
    @Company 公司:    kunlong@sz.com

	@Author  作者:    LEIJMDAS@163.COM  时间: 2024-05-05 13:32:16
	@Update  作者:    LEIJMDAS@163.COM  时间: 2024-05-05 13:32:16
*/

const singleNameFileInfoDto = "difactroy.FileInfoDto"

// init register load
func init() {
	registerBeanFileInfoDto()
}

// register FileInfoDto
func registerBeanFileInfoDto() {
	basedi.RegisterLoadBean(singleNameFileInfoDto, LoadFileInfoDto)
}

func FindBeanFileInfoDto() *FileInfoDto {
	return basedi.FindBean(singleNameFileInfoDto).(*FileInfoDto)
}

func LoadFileInfoDto() baseiface.ISingleton {
	var s = NewFileInfoDto()
	InjectFileInfoDto(s)
	return s

}

func InjectFileInfoDto(s *FileInfoDto) {

	logrus.Debug("inject")
}
