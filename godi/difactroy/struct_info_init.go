package difactroy

import (
	"gitee.com/webcli/goconfig/base/baseiface"
	"gitee.com/webcli/goconfig/basedi"
	"github.com/sirupsen/logrus"
)

/*
	@Title   文件名称: struct_info_init.go
	@Desp    描述:    自动注册注入
    @Company 公司:    kunlong@sz.com

	@Author  作者:    LEIJMDAS@163.COM  时间: 2024-05-05 13:32:16
	@Update  作者:    LEIJMDAS@163.COM  时间: 2024-05-05 13:32:16
*/

const singleNameStructInfo = "difactroy.StructInfo"

// init register load
func init() {
	registerBeanStructInfo()
}

// register StructInfo
func registerBeanStructInfo() {
	basedi.RegisterLoadBean(singleNameStructInfo, LoadStructInfo)
}

func FindBeanStructInfo() *StructInfo {
	return basedi.FindBean(singleNameStructInfo).(*StructInfo)
}

func LoadStructInfo() baseiface.ISingleton {
	var s = NewStructInfo()
	InjectStructInfo(s)
	return s

}

func InjectStructInfo(s *StructInfo) {

	logrus.Debug("inject")
}
