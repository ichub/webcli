package difactroy

import (
	"github.com/stretchr/testify/suite"
	"testing"
)

type TestDiSuite struct {
	suite.Suite
	DiFactroy *DiFactroy
}

func TestDiSuites(t *testing.T) {
	suite.Run(t, new(TestDiSuite))
}

func (this *TestDiSuite) SetupTest() {
	this.DiFactroy = NewDiFactroy()
	ichublog.InitLogrus()
}

func (this *TestDiSuite) Test001_MakeDi() {
	var dto = didto.NewDiDto()
	dto.PkgName = "difactroy"
	dto.StructName = "DiFactroy"
	dto.ForceBuild = true
	this.DiFactroy.MakeDi(dto)

}
