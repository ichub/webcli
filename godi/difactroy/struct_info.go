package difactroy

import (
	"gitee.com/webcli/goconfig/base/basedto"
	"gitee.com/webcli/godi/diconsts"
	"gitee.com/webcli/godi/didto"
	"gitee.com/webcli/godi/diutils"
	"strings"
)

type StructInfo struct {
	basedto.BaseEntity

	IsBaseEntiyStruct bool `json:"isBaseEntiyStruct"`
	*didto.DiDto

	MethodNames []string `json:"methodNames,omitempty"`
	Fields      []string `json:"fields"`
}

func NewStructInfo() *StructInfo {
	var si = &StructInfo{
		DiDto:       didto.NewDiDto(),
		MethodNames: make([]string, 0),
	}
	si.InitProxy(si)
	return si
}

func (this *StructInfo) NewFuncNameDefault() string {
	return "New" + this.StructName
}

func (this *StructInfo) checkBaseEntity() bool {
	for _, v := range this.Fields {
		this.IsBaseEntiyStruct = v == diconsts.BaseEntity || v == diconsts.BaseEntitySingle
		if this.IsBaseEntiyStruct {
			return true
		}
	}
	this.IsBaseEntiyStruct = diutils.FindKey(this.StructName)
	return this.IsBaseEntiyStruct
}

func (s *StructInfo) ParsePkgName(rootdir, basepkg string) {

	s.FullPkg = strings.Replace(s.PathFile, rootdir, basepkg, -1)
	s.FullPkg = strings.Replace(s.FullPkg, "\\", "/", -1)
	var paths = strings.Split(s.FullPkg, "/")
	s.FullPkg = strings.Join(paths[0:len(paths)-2], "/") + "/" + s.PkgName

}

func (s *StructInfo) AddMethodName(methodName string) {
	s.MethodNames = append(s.MethodNames, methodName)
}

func (s *StructInfo) AddField(fieldName string) {
	s.Fields = append(s.Fields, fieldName)
}
