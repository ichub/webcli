package difactroy

import (
	"gitee.com/webcli/goconfig/base/baseiface"
	"gitee.com/webcli/goconfig/basedi"
	"gitee.com/webcli/goconfig/ichubconfig"
	"github.com/sirupsen/logrus"
)

/*
	@Title   文件名称: di_factroy_init.go
	@Desp    描述:    自动注册注入
    @Company 公司:    kunlong@sz.com

	@Author  作者:    LEIJMDAS@163.COM  时间: 2024-05-05 13:32:16
	@Update  作者:    LEIJMDAS@163.COM  时间: 2024-05-05 13:32:16
*/

const singleNameDiFactroy = "difactroy.DiFactroy"

// init register load
func init() {
	registerBeanDiFactroy()
}

// register DiFactroy
func registerBeanDiFactroy() {
	basedi.RegisterLoadBean(singleNameDiFactroy, LoadDiFactroy)
}

func FindBeanDiFactroy() *DiFactroy {
	return basedi.FindBean(singleNameDiFactroy).(*DiFactroy)
}

func LoadDiFactroy() baseiface.ISingleton {
	var s = NewDiFactroy()
	InjectDiFactroy(s)
	return s

}

func InjectDiFactroy(s *DiFactroy) {

	// 自动实例化
	s.Config = ichubconfig.FindBeanIchubConfig()
	logrus.Debug("inject")
}
