package test

import (
	"fmt"
	"gitee.com/ichub/webcli/goconfig/base/baseutils/fileutils"
	"gitee.com/ichub/webcli/goconfig/base/baseutils/jsonutils"
	"gitee.com/ichub/webcli/goconfig/base/baseutils/stringutils"
	"gitee.com/ichub/webcli/goconfig/ichubconfig"
	"gitee.com/ichub/webcli/goconfig/ichublog"
	"gitee.com/ichub/webcli/goweb/pagedb"
	"gitee.com/ichub/webcli/goweb/pagees"
	"gitee.com/ichub/webcli/ichubengine/ichubcli"
	"gitee.com/ichub/webcli/ichubengine/ichubclicommon"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/suite"
	"testing"
)

// 依赖 suite.Suite
type TestWebEsQuerySuite struct {
	suite.Suite
	ruleClient *ichubcli.RuleClient
	//RuleClient
	value int
}

func (suite *TestWebEsQuerySuite) Check(exp interface{}, real interface{}, msg ...string) {
	fmt.Println("real=", real)
	expect := stringutils.ToStringWith(exp)
	r := stringutils.ToStringWith(real)
	suite.Equal(expect, r, msg)
}

// 用于 'go test' 的入口
func TestEsQuerySuites(t *testing.T) {
	suite.Run(t, new(TestWebEsQuerySuite))
}

// 每个测试运行前，会执行
func (suite *TestWebEsQuerySuite) SetupTest() {
	//baseutils.FindRootDir()
	ichublog.InitLogrus()

	suite.value = 5
	suite.T().Log("SetupTest")
	var webClientCfg = ichubconfig.New(ichubconfig.ConfigfileIchubengine)
	var webClientDto = webClientCfg.ReadIchubWebClient()
	suite.ruleClient = ichubcli.New(webClientDto)

}
func (suite *TestWebEsQuerySuite) TearDownTest() {
	suite.value = 5
	//	suite.T().Log("TearDownTest")
}
func (suite *TestWebEsQuerySuite) SetupSuite() {
	logrus.Println("Suite setup")
	logrus.Println("领域驱动设计，测试驱动开发!")
	fileutils.FindRootDir()
}
func (suite *TestWebEsQuerySuite) TearDownSuite() {
	fmt.Println("Suite teardown")
}
func (suite *TestWebEsQuerySuite) Test001_PostRuleFuncParamsDepth() {
	const param = ` {
		"shop_id": 0,
		"object_id" : 0,
		"object_type" : "1"
	}`
	params := ichubclicommon.NewIchubClientParams()
	params.Put("shop_id", 10)
	params.Put("object_id", 10)
	params.Put("object_type", 1)
	ps := ichubclicommon.NewIchubClientParams()
	ps.Put("attr1", 1)
	ps.Put("attr21", 21)
	ps.Put("attr31", 31)
	params.Put("attr", ps.Params)

	logrus.Info(params.ToParams())
	//test
	result := suite.ruleClient.PostRuleFunc(ichubclicommon.FUNC_CONFIG, params.ToParams())

	//check
	suite.Equal(200, result.Code)
	logrus.Info(result)
}

func (suite *TestWebEsQuerySuite) Test002_EsClientQuery() {
	//req
	var req = pagees.NewEsRequest(3, 1)
	req.IndexName = "ichub_sys_dept"
	// esClient.Eq("dept_id", "553251751520632832")
	// esClient.OrderBy("dept_id", "desc")
	//	esClient.Like("email", "s@236")
	req.Like("email", "leijmdas@163.com")
	req.Between("dept_id", []interface{}{"553263667185975296", "553263667185975296"})
	req.In("dept_id", []interface{}{"553263667185975296", "553263667185975296"})

	//test
	result := suite.ruleClient.EsClientQuery(req)

	//check
	suite.Equal(200, result.Code)
	ichublog.IchubLog.Println(result)
	logrus.Info(result)
}
func (suite *TestWebEsQuerySuite) Test0003_PostRuleFunclist() {

	//test
	result := suite.ruleClient.PostRuleFunc(ichubclicommon.FUNC_LISTFUNCS, ichubclicommon.FUNC_PARAM_EMPTY)

	//check
	suite.Equal(200, result.Code)
	fmt.Println(jsonutils.ToJsonPretty(result.Data))
}
func (suite *TestWebEsQuerySuite) Test004_DbRequest() {

	var dbRequest = pagedb.NewIchubPageDbRequest()
	dbRequest.PageSize = 2
	dbRequest.TableName = "rules"

	dbRequest.Ge("rule_id", 3)
	dbRequest.OrderBy("rule_id", "asc")

	var result = dbRequest.GeneralQuery()
	logrus.Info(result.ToPrettyString())
	ichublog.Log(result)
	suite.Equal(200, result.Code)

}
