package test

import (
	"fmt"
	"gitee.com/ichub/webcli/goconfig/base/baseutils/fileutils"
	"gitee.com/ichub/webcli/goconfig/base/baseutils/jsonutils"
	"gitee.com/ichub/webcli/goconfig/base/baseutils/stringutils"
	"gitee.com/ichub/webcli/goconfig/ichubconfig"
	"gitee.com/ichub/webcli/goconfig/ichublog"
	"gitee.com/ichub/webcli/goweb/pagedb"
	"gitee.com/ichub/webcli/ichubengine/ichubcli"
	"gitee.com/ichub/webcli/model"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/suite"
	"testing"
)

// 依赖 suite.Suite
type TestWebDbQuerySuite struct {
	suite.Suite
	ruleClient *ichubcli.RuleClient
}

func (suite *TestWebDbQuerySuite) Check(exp interface{}, real interface{}, msg ...string) {
	fmt.Println("real=", real)
	expect := stringutils.ToStringWith(exp)
	r := stringutils.ToStringWith(real)
	suite.Equal(expect, r, msg)
}

// 用于 'go test' 的入口
func TestDbQuerySuites(t *testing.T) {
	suite.Run(t, new(TestWebDbQuerySuite))
}

// 每个测试运行前，会执行
func (suite *TestWebDbQuerySuite) SetupTest() {
	//baseutils.FindRootDir()
	ichublog.InitLogrus()

	suite.T().Log("SetupTest")
	var webClientCfg = ichubconfig.New(ichubconfig.ConfigfileIchubengine)
	var webClientDto = webClientCfg.ReadIchubWebClient()
	suite.ruleClient = ichubcli.New(webClientDto)

}
func (suite *TestWebDbQuerySuite) TearDownTest() {

	//	suite.T().Log("TearDownTest")
}
func (suite *TestWebDbQuerySuite) SetupSuite() {
	logrus.Println("Suite setup")
	logrus.Println("领域驱动设计，测试驱动开发!")
	fileutils.FindRootDir()
}
func (suite *TestWebDbQuerySuite) TearDownSuite() {
	fmt.Println("Suite teardown")
}
func (suite *TestWebDbQuerySuite) Test001_DbRequestEntity() {

	var dbRequest = pagedb.NewIchubPageDbRequest()
	dbRequest.PageSize = 2
	dbRequest.TableName = "employee"
	dbRequest.TimeToInt = false
	var result = dbRequest.GeneralQuery()

	suite.Equal(200, result.Code)
	var emps = result.Data.([]map[string]interface{})
	var res = jsonutils.ToJsonPretty(emps)
	var emp []model.Employee
	jsonutils.FromJson(res, &emp)
	result.To(&emp)
	ichublog.Log("dbquery*****=", jsonutils.ToJsonPretty(emp))
}
func (suite *TestWebDbQuerySuite) Test002_DbRequestDto() {

	var dbRequest = pagedb.NewIchubPageDbRequest()
	dbRequest.PageSize = 2
	dbRequest.TableName = "employee"
	dbRequest.TimeToInt = true
	var result = dbRequest.GeneralQuery()
	suite.Equal(200, result.Code)
	var emps = result.Data.([]map[string]interface{})
	var res = jsonutils.ToJsonPretty(emps)
	var outemp []model.EmployeeDto
	jsonutils.FromJson(res, &outemp)
	//result.To(&outemp)
	ichublog.Log("dbquery dto*****=", jsonutils.ToJsonPretty(outemp))
}
