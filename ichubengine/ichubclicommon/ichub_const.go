package ichubclicommon

const (
	FUNC_PARAM_EMPTY = "{}"
	FUNC_CONFIG      = "func_config"

	FUNC_DBQUERY   = "func_dbquery"
	FUNC_ESQUERY   = "func_esquery"
	FUNC_LISTFUNCS = "func_listfuncs"
	FUNC_LISTFUNC  = "func_listfunc"
	FUNC_SNOWFLAKE = "func_snowflake"

	FUNC_DEMO = "func_demo"
)
const (
	All_Addr       = "/alladdr"
	POST_Rule      = "/ichubengine"
	POST_RuleBatch = "/ichubengineBatch"
)
