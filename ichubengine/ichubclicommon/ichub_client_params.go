package ichubclicommon

import (
	"gitee.com/ichub/webcli/goconfig/base/basedto"
	"gitee.com/ichub/webcli/goconfig/base/baseutils/jsonutils"
)

type IchubClientParams struct {
	basedto.BaseEntity
	Params map[string]interface{}
}

func NewIchubClientParams() *IchubClientParams {
	ps := &IchubClientParams{}
	ps.InitProxy(ps)
	ps.Params = make(map[string]interface{})
	return ps
}

func (this *IchubClientParams) Put(key string, value interface{}) {
	this.Params[key] = value
}
func (this *IchubClientParams) Get(key string) interface{} {
	return this.Params[key]
}

func (this *IchubClientParams) ToParams() string {
	return jsonutils.ToJsonPretty(this.Params)
}
