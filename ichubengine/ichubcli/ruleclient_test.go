package ichubcli

import (
	"fmt"
	"gitee.com/ichub/webcli/goweb/webclient"
	"github.com/imroc/req"

	"gitee.com/ichub/webcli/goconfig/base/basedto"
	"gitee.com/ichub/webcli/goconfig/ichubconfig"
	"gitee.com/ichub/webcli/goconfig/ichubcontext"
	"gitee.com/ichub/webcli/goconfig/ichublog"
	"gitee.com/ichub/webcli/ichubengine/ichubclicommon"

	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/suite"
	"os"
	"testing"
)

/*
@Title    文件名称: ruleserver_test.go
@Description  描述: 规则引擎测试套

@Author  作者: leijianming@163.com  时间(2024-02-18 22:38:21)
@Update  作者: leijianming@163.com  时间(2024-02-18 22:38:21)
*/

type TestIchubServerSuite struct {
	suite.Suite
	ruleClient *RuleClient
}

func TestServerSuites(t *testing.T) {
	suite.Run(t, new(TestIchubServerSuite))
}

// 每个测试运行前，会执行
func (suite *TestIchubServerSuite) SetupTest() {
	ichublog.InitLogrus()
	os.Setenv("BasePath", ichubcontext.CommonContext.BasePath)

	//suite.ruleClient.SetTestUrl(LocalBaseUrl)
	logrus.Println("SetupTest...")
	var webClientCfg = ichubconfig.New(ichubconfig.ConfigfileIchubengine)
	var webClientDto = webClientCfg.ReadIchubWebClient()
	suite.ruleClient = New(webClientDto)
	logrus.Println("SetupTest...")
}
func (suite *TestIchubServerSuite) TearDownTest() {

}
func (suite *TestIchubServerSuite) SetupSuite() {
	logrus.Println("Suite setup")
	logrus.Println("领域驱动设计，测试驱动开发!")

}

func (suite *TestIchubServerSuite) TearDownSuite() {
	logrus.Println("Suite teardown")
}

const rule1 = `//name_rule003_pay 计算工资
 
//author: leijmdas
//date: 2024-2-8

//input  param :
//  Base Bonus Tax
//return
//  Pay = Base+Bonus-Tax

rule "pay-simple-compute-001" "简单计算工资示例" salience 0
begin
    
    //底薪
    base=In.GetParam("Base")
    //奖金
    bonus=In.GetParam("Bonus")
    //扣税
    tax=In.GetParam("Tax")
    //实发
    pay=base.ToFloat64()+bonus.ToFloat64()-tax.ToFloat64()

    Out.SetReturnValue(0)
    Out.SetReturnMsg("计算成功！")
    Out.SetParam("Pay",pay) 
	IchubLog("Out=",Out) 

end`

const param1 = `{
			"Base": 10000,
			"Bonus": 5000,
			"Tax": 1000
		}`

func (suite *TestIchubServerSuite) Test0001_AllAddr() {

	//test
	result := suite.ruleClient.GetAllAddr()
	//Checks
	result.Log()
	suite.Equal(200, result.Code)

}

func (suite *TestIchubServerSuite) Test0002_PostRule() {

	//prepare
	var ruleData = &ichubclicommon.RuleData{
		Param: param1,
		Rule:  rule1,
	}
	logrus.Info("ruleData = ", ruleData)
	//test
	result := suite.ruleClient.PostRule(ruleData)

	//Checks
	suite.Equal(200, result.Code)
	result.Checks(suite.Suite, "Pay=14000|Pay=14000")
	ichublog.Log(result.ToPrettyString())
}

func (suite *TestIchubServerSuite) Test0003_PostRuleBatch() {

	//prepare
	var ruleData = ichubclicommon.NewRuleData(rule1, param1)
	var data = []*ichubclicommon.RuleData{ruleData, ruleData}

	//test
	var results = suite.ruleClient.PostRuleBatch(data)

	//Checks
	suite.Equal(200, results.Code)
	if len(results.Data) > 0 {
		results.Check(suite.Suite, 14000, results.Data[0].ValueByKey("Pay"))
	}
	logrus.Info(results)

}

func (suite *TestIchubServerSuite) Test0004_PostPay2RuleServer() {
	//prepare
	const param2 = `{
		"Base": 20000,
		"Bonus": 25000,
		"Tax": 20000
	}`
	var ruleData = &ichubclicommon.RuleData{
		Param: param2,
		Rule:  rule1,
	}
	//test
	resp, err := suite.ruleClient.Post2RuleServer(ruleData)
	if err != nil {
		fmt.Println(err)
		return
	}
	//Checks
	suite.Equal(200, resp.Response().StatusCode)
	suite.ruleClient.Log(resp)
	suite.ruleClient.Checks(suite.Suite, resp, 25000)

}

/*golang 里的 http 标准库，发起 http 请求时，写法比较繁琐。所以智慧又“偷懒的”程序员们，
发挥自己的创造力，写出了一些好用的第三方库，这里介绍其中的一个 http 库：go-resty*/
// https://www.cnblogs.com/jiujuan/p/14583605.html
func (suite *TestIchubServerSuite) Test0005_restyPostPay4RuleServer() {
	//client := resty.New() // 创建一个restry客户端

	//resp, err := client.R().EnableTrace().Get("https://httpbin.org/get")
	var ruleData = &ichubclicommon.RuleData{
		Param: param1,
		Rule:  rule1,
	}
	resp, err := req.New().Post(localUrl, ruleData.ToString())
	if err != nil {
		fmt.Println(err)
		return
	}
	//suite.ruleClient.Log(resp)
	suite.Equal(200, resp.Response().StatusCode)
	var result = basedto.IchubResultOf(resp)
	result.CheckCode(suite.Suite, 200)
	result.Checks(suite.Suite, "Pay=14000|ReturnValue=0")
}

func (suite *TestIchubServerSuite) Test0006_restyPostPay5RuleServer() {

	const p = `{
		"Base": 20000,
		"Bonus": 25000,
		"Tax": 20000
	}`
	var ruleData = &ichubclicommon.RuleData{
		Param: p,
		Rule:  rule1,
	}
	result := suite.ruleClient.Post(ruleData.ToString(), localUrl)

	suite.Equal(200, result.Code)
	fmt.Println(result)

}

func (suite *TestIchubServerSuite) Test0007_restyGetEnhance() {

}

func (suite *TestIchubServerSuite) Test0008_restyPostDatas2Rule() {

	//prepare
	var ruleData = &ichubclicommon.RuleData{
		Rule:  rule1,
		Param: param1,
	}
	//test
	resps, err := suite.ruleClient.Post2RuleServerDatas([]*ichubclicommon.RuleData{ruleData, ruleData})
	if err != nil {
		fmt.Println(err)
		suite.Equal(true, false, err)
		return
	}
	suite.Equal(200, 200)
	suite.ruleClient.Log(resps[0])

	suite.ruleClient.Checks(suite.Suite, resps[0], 14000)
}

func (suite *TestIchubServerSuite) Test0009_GetServiceAddrs() {

	var webclient = webclient.NewIchubWebClient()

	ss := webclient.GetServiceAddrs()
	s := webclient.GetServiceAddr()
	fmt.Println(s)
	fmt.Println(ss)

}

func (suite *TestIchubServerSuite) Test0010_homedir() {

	home := os.Getenv("HOME")
	fmt.Println(home)
}

func (suite *TestIchubServerSuite) Test0011_getAddr() {

	suite.ruleClient.Init()
	addr := suite.ruleClient.GetServiceAddr()
	ichublog.IchubLog.Println("addr=", addr)

	fmt.Println(addr)
}

func (suite *TestIchubServerSuite) Test0012_HelloWorld() {

	//test
	result := suite.ruleClient.HelloWorld()
	//Checks
	result.Log()

	suite.Equal(200, result.Code)

}

func (suite *TestIchubServerSuite) Test0013_PostRuleDefault() {

	//test
	result := suite.ruleClient.PostDefault()
	//Checks
	result.Log()

	suite.Equal(200, result.Code)

	//suite.ruleClient.CheckResult(suite.T(), result, 14000)
	suite.Equal(float64(14000), result.ValueByKey("Pay"))
}

func (suite *TestIchubServerSuite) Test0014_PostRuleBatch() {

	var ruleClient = suite.ruleClient
	//prepare
	var ruleData = &ichubclicommon.RuleData{
		Param: param1,
		Rule:  rule1,
	}
	ichublog.IchubLog.Println("ruleData=", ruleData)
	var datas = []*ichubclicommon.RuleData{ruleData, ruleData}

	//test
	var results = ruleClient.PostRuleBatch(datas)

	//Checks
	suite.Equal(200, results.Code)
	//suite.Equal(float64(14000), results.Data[0].ValueByKey("Pay"))
	results.Log() //fmt.Println(results.String())
}
