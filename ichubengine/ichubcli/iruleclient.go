package ichubcli

import (
	"gitee.com/ichub/webcli/goconfig/base/basedto"
	"gitee.com/ichub/webcli/ichubengine/ichubclicommon"
)

type IruleClient interface {
	PostRule(ruleData *ichubclicommon.RuleData) *basedto.IchubResult
	PostRuleBatch(ruleDatas []*ichubclicommon.RuleData) *basedto.IchubResults
}
