package ichubcli

import (
	"fmt"
	"gitee.com/ichub/webcli/goconfig/base/baseconfig"
	"gitee.com/ichub/webcli/goconfig/base/basedto"
	"gitee.com/ichub/webcli/goconfig/base/baseutils"
	"gitee.com/ichub/webcli/goconfig/base/baseutils/jsonutils"
	"gitee.com/ichub/webcli/goconfig/ichublog"
	"gitee.com/ichub/webcli/goweb/pagedb"
	"gitee.com/ichub/webcli/goweb/pagees"
	"gitee.com/ichub/webcli/goweb/webclient"
	"gitee.com/ichub/webcli/ichubengine/ichubclicommon"
	"github.com/imroc/req"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/suite"
)

//https://www.cnblogs.com/feixiang-energy/p/15572292.html
//第三代规则引擎

/*
	@Title    文件名称: ruleserver_client.go
	@Description  描述: 规则引擎客户端

	@Author  作者: leijianming@163.com  时间(2024-02-18 22:38:21)
	@Update  作者: leijianming@163.com  时间(2024-02-18 22:38:21)
*/

type RuleClient struct {
	Url, Urls, AllAddrUrl, HelpUrl string

	*webclient.IchubWebClient
}

func NewRuleClient() *RuleClient {
	return &RuleClient{

		Url:            localUrl,
		Urls:           localUrls,
		AllAddrUrl:     localAllAddrUrls,
		IchubWebClient: webclient.NewIchubWebClient(),
	}

}
func New(dto *baseconfig.WebClientDto) *RuleClient {
	return &RuleClient{

		Url:            localUrl,
		Urls:           localUrls,
		AllAddrUrl:     localAllAddrUrls,
		IchubWebClient: webclient.NewWebClient(dto),
	}

}
func (client *RuleClient) String() string {
	return jsonutils.ToJsonPretty(client)
}

func (ruleClient *RuleClient) init() *RuleClient {

	ruleClient.IchubWebClient.Init()
	ruleClient.RefreshAddr()
	return ruleClient
}

func (ruleClient *RuleClient) RefreshAddr() {

	ruleClient.BaseUrl = ruleClient.GetServiceAddr()
	ruleClient.SetBaseUrl(ruleClient.BaseUrl)
}

/*
baseurl="http://localhost:88"
*/
func (ruleClient *RuleClient) SetBaseUrl(baseUrl string) *RuleClient {
	baseUrl = ruleClient.IchubWebClient.SetBaseUrl(baseUrl)

	ruleClient.Url = baseUrl + ichubclicommon.POST_Rule
	ruleClient.Urls = baseUrl + ichubclicommon.POST_RuleBatch
	ruleClient.AllAddrUrl = baseUrl + ichubclicommon.All_Addr
	ruleClient.HelpUrl = baseUrl + "/dohelp"
	return ruleClient
}

func (ruleClient *RuleClient) PostDefault() *basedto.IchubResult {

	var ruleData = &ichubclicommon.RuleData{
		Param: param,
		Rule:  rule,
	}
	ruleClient.RefreshAddr()

	return ruleClient.Post(ruleData.ToString(), ruleClient.Url)

}
func (this *RuleClient) PostRuleESQuery(Param string) (result *basedto.IchubResult) {

	return this.PostRuleFunc(ichubclicommon.FUNC_ESQUERY, Param)

}
func (this *RuleClient) EsClientQuery(esReq *pagees.IchubPageEsRequest) (result *basedto.IchubResult) {

	return this.PostRuleFunc(ichubclicommon.FUNC_ESQUERY, esReq.ToPrettyString())

}
func (this *RuleClient) DbClientQuery(dbReq *pagedb.IchubPageDbRequest) (result *basedto.IchubResult) {

	return this.PostRuleFunc(ichubclicommon.FUNC_DBQUERY, dbReq.ToPrettyString())

}
func (this *RuleClient) PostRuleFunc(ruleId string, Param string) (result *basedto.IchubResult) {

	var ruleData ichubclicommon.RuleData
	ruleData.Id = ruleId
	ruleData.Param = Param

	if ruleData.Param == "" {
		ruleData.Param = "{}"
	}
	return this.PostRule(&ruleData)
}

// 执行单个规则
func (ruleClient *RuleClient) PostRule(ruleData *ichubclicommon.RuleData) (result *basedto.IchubResult) {
	ruleClient.RefreshAddr()
	ruleData.Rule2IdName()

	ichublog.IchubLog.Println(ruleData)
	result = ruleClient.Post(ruleData.ToString(), ruleClient.Url)

	return
}

// 执行多个规则  批量接口
func (ruleClient *RuleClient) PostRuleBatch(ruleDatas []*ichubclicommon.RuleData) (results *basedto.IchubResults) {
	logrus.Println("PostRuleBatch start urls =", ruleClient.Urls)

	for _, ruleData := range ruleDatas {

		logrus.Println(ruleData.Param)
		ruleData.Rule2IdName()
		logrus.Info(ruleData.ToLines())
	}
	ruleClient.RefreshAddr()
	ichublog.IchubLog.Println(ruleDatas)

	results = ruleClient.PostBatch(jsonutils.ToJsonPretty(ruleDatas), ruleClient.Urls)
	logrus.Println("PostRuleBatch end urls =", ruleClient.Urls)

	ichublog.IchubLog.Println(results)
	return
}

func (ruleClient *RuleClient) GetAllAddr() (result *basedto.IchubResult) {
	fmt.Println("GetAllAddr url =", ruleClient.AllAddrUrl)
	ichublog.IchubLog.Println("GetAllAddr url =", ruleClient.AllAddrUrl)

	ruleClient.RefreshAddr()

	result = ruleClient.Get(ruleClient.AllAddrUrl)
	fmt.Println("GetAllAddr result=", result.ToString())
	ichublog.IchubLog.Println("GetAllAddr result=", result.ToString())

	return
}

func (ruleClient *RuleClient) HelloWorld() (result *basedto.IchubResult) {
	fmt.Println("HelloWolrd url =", ruleClient.BaseUrl)
	ichublog.IchubLog.Println("HelloWolrd url =", ruleClient.BaseUrl)

	ruleClient.RefreshAddr()

	result = ruleClient.Get(ruleClient.BaseUrl)
	fmt.Println("HelloWolrd result=", result.ToString())
	ichublog.IchubLog.Println("HelloWolrd result=", result.ToString())

	return
}
func (ruleClient *RuleClient) Help() (result *basedto.IchubResult) {

	ichublog.IchubLog.Println("help url =", ruleClient.BaseUrl+"/help")

	ruleClient.RefreshAddr()
	result = ruleClient.Get(ruleClient.BaseUrl + "/help")

	fmt.Println("help result=", result.ToString())
	ichublog.IchubLog.Println("help result=", result.ToString())

	return
}
func (ruleClient *RuleClient) DoHelp() (result *basedto.IchubResult) {

	ichublog.IchubLog.Println("help url =", ruleClient.HelpUrl)

	ruleClient.RefreshAddr()
	result = ruleClient.Get(ruleClient.HelpUrl)

	fmt.Println("help result=", result.ToString())
	ichublog.IchubLog.Println("help result=", result.ToString())

	return
}

func (ruleClient *RuleClient) Post2RuleServer(ruleData *ichubclicommon.RuleData) (resp *req.Resp, err error) {
	ruleClient.RefreshAddr()

	resp, err = req.New().Post(ruleClient.Url, ruleData.ToString())

	return

}
func (ruleClient *RuleClient) Post2RuleServerDatas(ruleDatas []*ichubclicommon.RuleData) (resps []*req.Resp, errret error) {
	for _, ruleData := range ruleDatas {
		resp, err := ruleClient.Post2RuleServer(ruleData)
		resps = append(resps, resp)
		if err != nil {

			fmt.Println(err)
			return resps, err
		}
	}

	return resps, nil

}

func (ruleClient *RuleClient) CheckResult(suite suite.Suite, ichubResult *basedto.IchubResult, exp float64) {

	var pay = ichubResult.ValueByKey("Pay")
	ichublog.IchubLog.Println("\r\nichubResult=", ichubResult.ToString())
	ichubResult.Checks(suite, baseutils.Any2Str(pay)+"="+baseutils.Any2Str(exp))
}

func (ruleClient *RuleClient) Checks(suite suite.Suite, resp *req.Resp, exp interface{}) {
	ichubResult, err := basedto.NewIchubResult().FromJson(resp.Bytes())
	if err != nil {
		suite.Equal(true, false, err)
		return
	}
	ichubResult.Check(suite, "Pay="+baseutils.Any2Str(exp))
}

func (ruleClient *RuleClient) Log(resp *req.Resp) {
	// Explore response object

	fmt.Println()

	ichublog.IchubLog.Println("body:=", *resp)
	// Explore trace info
	fmt.Println("Request Trace Info:")

}
