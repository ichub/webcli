package ichubcli

/*
@Title    文件名称: rulesclient_const.go
@Description  描述: 规则引擎客户端

@Author  作者: leijianming@163.com  时间(2024-02-18 22:38:21)
@Update  作者: leijianming@163.com  时间(2024-02-18 22:38:21)
*/
const (
	LocalBaseUrl     = "http://localhost:88"
	localUrl         = "http://localhost:88/ichubengine"
	localUrls        = "http://localhost:88/ichubengineBatch"
	localAllAddrUrls = "http://localhost:88/alladdr"
	etcdhost         = "192.168.4.111:2379"
	servername       = "web.ichubengine.ichub.com"
)
const (
	CMD_alladdr = "alladdr"
)

const (
	rule = `//name_rule003_pay 计算工资
//in out 参数
//author: leijmdas
//date: 2024-2-8

//input  param :
//  Base
//   Bonus
//   Tax
//return
//    Pay = Base+Bonus-Tax


rule "pay-simple-compute-001" "简单计算工资示例" salience 0
begin
    Out.SetReturnMsg("计算失败！")
    //底薪
    base=In.GetParam("Base")
    //奖金
    bonus=In.GetParam("Bonus")
    //扣税
    tax=In.GetParam("Tax")
    //实发
    pay=base.ToFloat64()+bonus.ToFloat64()-tax.ToFloat64()

    Out.SetReturnValue(0)
    Out.SetReturnMsg("计算成功！")
    Out.SetParam("Pay",pay)

end`

	param = `{
			"Base": 10000,
			"Bonus": 5000,
			"Tax": 1000
		}`
)
