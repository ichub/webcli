package enccmd

import (
	"gitee.com/ichub/webcli/goconfig/ichublog"
	"github.com/spf13/cobra"
)

func BuildCmd(rootcmd *cobra.Command) {
	ichublog.InitLogrus()
	var decCmd = &cobra.Command{
		Use:   "dec [flag]",
		Short: "dec password flag of db",
		Run: func(cmd *cobra.Command, args []string) {
			//flag := cmd.Flags().Arg(0)
			//pwd := encrypt.AesDecBase64(flag)
			//fmt.Println(pwd)
		},
	}

	var encCmd = &cobra.Command{
		Use:   "enccmd [flag]",
		Short: "enccmd password of db",
		Run: func(cmd *cobra.Command, args []string) {
			//flag := cmd.Flags().Arg(0)
			//pwd := encrypt.AesEncBase64(flag)
			//fmt.Println(pwd)
		},
	}
	rootcmd.AddCommand(decCmd)
	rootcmd.AddCommand(encCmd)

}
