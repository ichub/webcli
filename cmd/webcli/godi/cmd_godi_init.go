package godi

import (
	"github.com/sirupsen/logrus"
)

/*
	@Title   文件名称: cmd_godi_init.go
	@Desp    描述:    自动注册注入
    @Company 公司:    kunlong@sz.com

	@Author  作者:    LEIJMDAS@163.COM  时间: 2024-05-05 13:32:15
	@Update  作者:    LEIJMDAS@163.COM  时间: 2024-05-05 13:32:15
*/

const singleNameCmdGodi = "godi.CmdGodi"

// init register load
func init() {
	registerBeanCmdGodi()
}

// register CmdGodi
func registerBeanCmdGodi() {
	basedi.RegisterLoadBean(singleNameCmdGodi, LoadCmdGodi)
}

func FindBeanCmdGodi() *CmdGodi {
	return basedi.FindBean(singleNameCmdGodi).(*CmdGodi)
}

func LoadCmdGodi() baseiface.ISingleton {
	var s = NewCmdGodi()
	InjectCmdGodi(s)
	return s

}

func InjectCmdGodi(s *CmdGodi) {

	logrus.Debug("inject")
}
