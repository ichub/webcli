package godi

type CmdGodi struct {
	basedto.BaseEntitySingle
	DiFactroy *difactroy.DiFactroy
}

func NewCmdGodi() *CmdGodi {
	return &CmdGodi{
		DiFactroy: difactroy.FindBeanDiFactroy(),
	}
}

func (c *CmdGodi) Exec() string {

	c.DiFactroy.MakeDiAllForce(true)
	return "godi"
}
