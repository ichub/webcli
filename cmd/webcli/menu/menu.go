package menu

import (
	"fmt"
	"gitee.com/ichub/webcli/cmd/webcli/enccmd"
	"gitee.com/ichub/webcli/godi/difactroy"
	"github.com/spf13/cobra"
)

var version = "1.0.0"
var rootCmd = &cobra.Command{
	Use:   "ichubengine-server",
	Short: "A ichubengine-server : atool  of webmenu",
	Long:  `This is a godi application using Cobra`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("Hello, webserver!")
	},
}

var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "Print the version number of webserver",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Printf("webserver version %s\n", version)
	},
}
var forceCmd = &cobra.Command{
	Use:   "force",
	Short: "force MakeDi all struct",
	Run: func(cmd *cobra.Command, args []string) {
		difactroy.FindBeanDiFactroy().MakeDiAllForce(true)
	},
}
var allCmd = &cobra.Command{
	Use:   "all",
	Short: "MakeDi all struct",
	Run: func(cmd *cobra.Command, args []string) {
		difactroy.FindBeanDiFactroy().MakeDiAll()
	},
}
var fileCmd = &cobra.Command{
	Use:   "file [flag]",
	Short: "MakeDi one file flag",
	Run: func(cmd *cobra.Command, args []string) {

		flag := cmd.Flags().Arg(0)
		difactroy.FindBeanDiFactroy().MakeDiFile(flag)
	},
}
var struCmd = &cobra.Command{
	Use:   "stru [flag]",
	Short: "MakeDi one struct flag",
	Run: func(cmd *cobra.Command, args []string) {
		flag := cmd.Flags().Arg(0)
		difactroy.FindBeanDiFactroy().MakeDiStru(flag)
	},
}
var esServerCmd = &cobra.Command{
	Use:   "es",
	Short: "Start es server",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("Start es server")
		///	server.StartEsServer()
	},
}

var EngineCmd = &cobra.Command{
	Use:   "engine",
	Short: "Start engine server",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("Start engine server")
		//		server.StartEngineServer()
	},
}

var FactroyCmd = &cobra.Command{
	Use:   "factroy",
	Short: "Start factroy server",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("Start factroy server")
		//	server.StartFactroyServer()
	},
}

func init() {

	rootCmd.AddCommand(versionCmd)
	rootCmd.AddCommand(forceCmd)
	rootCmd.AddCommand(allCmd)
	rootCmd.AddCommand(fileCmd)
	rootCmd.AddCommand(struCmd)
	rootCmd.AddCommand(esServerCmd)
	rootCmd.AddCommand(EngineCmd)
	rootCmd.AddCommand(FactroyCmd)
	enccmd.BuildCmd(rootCmd)
}

func Execute() {
	rootCmd.Execute()
}
