package baseconsts

type RetCode int32

const IchubBasePath = "BasePath"
const ICHUBENGINGE_LOG_FILE = "/opt/logs/ichubengine_client.log"
const (
	ENV_EMPTY   = ""
	ENV_DEV     = "dev"
	ENV_TEST    = "test"
	ENV_RELEASE = "release"
	ENV_MASTER  = "master"

	DB_TYPE_MYSQL     = "mysql"
	DB_TYPE_POSTGRES  = "postgres"
	DB_TYPE_COCKROACH = "cockroach"

	RetCode_SUCCESS RetCode = 200
	RetCode_ERROR   RetCode = 500

	DATETIME_UTC = iota
	DATETIME_GMT8

	ZERO_TIME_INT64 = -62135596800
	PAGE_SIZE_ALL   = -1
	PAGE_CURRENT    = 1

	PAGE_SIZE_ZERO = 0

	PAGE_SIZE_DEFAULT = 20

	PAGE_SIZE_MAX = 500

	FormatDate     = "2006-01-02"
	FormatDateTime = "2006-01-02 15:04:05"
	FormatUTCTime  = "2006-01-02T15:04:05.000Z"
)
