package reflectutils

import (
	"fmt"
	"github.com/emirpasic/gods/maps/linkedhashmap"
	"reflect"
)

//gorm

type ReflectTags struct {
	tagMap *linkedhashmap.Map
}

func NewReflectTags() *ReflectTags {
	return &ReflectTags{
		tagMap: linkedhashmap.New(),
	}

}
func (tag *ReflectTags) ParseOne(v any, FieldName string, tagName string) {
	elem := reflect.TypeOf(v).Elem()

	structField, _ := elem.FieldByName(FieldName)
	tag.tagMap.Put(FieldName, structField.Tag.Get(tagName))

}

func (tag *ReflectTags) ToString() string {
	v, _ := tag.tagMap.ToJSON()
	return string(v)
}

func (tag *ReflectTags) Log() {

	//fmt.Println(tag.ToString())
	tag.tagMap.Each(func(key interface{}, value interface{}) {
		fmt.Println(key, value)
	})
	tag.tagMap.Map(func(key1 interface{}, value1 interface{}) (interface{}, interface{}) {
		return key1, value1
	})
}

func (tag *ReflectTags) ParseTags(v interface{}) {
	elem := reflect.TypeOf(v).Elem()

	for i := 0; i < elem.NumField(); i++ {
		structField := elem.Field(i)
		tag.tagMap.Put(structField.Name, structField.Tag.Get("gorm"))
	}
}
