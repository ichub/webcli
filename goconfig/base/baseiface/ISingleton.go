package baseiface

// NewBean
type ISingleton interface {
	Single() bool

	InitProxy(some any)
}
