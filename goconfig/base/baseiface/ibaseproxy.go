package baseiface

type IbaseProxy interface {
	ISingleton
	IBinding

	String() string

	ToString() string
	Log()
	Clone() any

	FromJson(body []byte) interface{}
	ToJson() string
	ToJsonBytes() []byte
	ValueOf(another interface{})
}
