package baseconfig

type ElasticClientDto struct {
	IchubClientDto
	URL string `json:"url"` //("http://192.168.4.111:9200,"),
	// 设置基于http base auth验证的账号和密码
	//elastic.SetBasicAuth("elastic", "123456"),
	Username string `json:"username"`
	Password string `json:"password"`
	// 启用gzip压缩
	Gzip bool `json:"gzip"`
}

func NewElasticClientDto() *ElasticClientDto {
	var client = &ElasticClientDto{
		Gzip: false,
	}

	client.InitProxy(client)
	return client
}
func (this *ElasticClientDto) Parse() *ElasticClientDto {
	this.URL = this.ParseValue("URL", this.URL)
	this.Username = this.ParseValue("Username", this.Username)
	this.Password = this.ParseValue("Password", this.Password)
	return this
}
