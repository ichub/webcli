package baseconfig

/*
@Title    文件名称: WebServerDto.go
@Description  描述: Web服务配置信息

@Author  作者: leijianming@163.com  时间(2024-02-18 22:38:21)
@Update  作者: leijianming@163.com  时间(2024-02-18 22:38:21)
*/
type WebClientDto struct {
	WebServerDto
	//used by client
	TestUrl string `json:"test_url"`
}

func NewWebClientDto(webServerDto WebServerDto) *WebClientDto {
	return &WebClientDto{WebServerDto: webServerDto}
}
func NewWebClient() *WebClientDto {
	return &WebClientDto{}
}

func (this *WebClientDto) Parse() *WebClientDto {
	this.WebServerDto.Parse()
	this.TestUrl = this.ParseValue("KEY", this.TestUrl)

	return this
}
