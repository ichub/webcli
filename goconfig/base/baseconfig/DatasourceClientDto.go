package baseconfig

type DatasourceClientDto struct {
	IchubClientDto

	Dbtype string `json:"dbtype"`
	Dbname string `json:"dbname"`
	Host   string `json:"host"`
	Port   string `json:"port"`

	Username string `json:"username"`
	Password string `json:"password"`
	Charset  string `json:"charset"`
}

func NewDatasourceClientDto() *DatasourceClientDto {
	var ds = &DatasourceClientDto{}
	ds.InitProxy(ds)
	return ds
}
func (this *DatasourceClientDto) Parse() *DatasourceClientDto {
	this.Dbtype = this.ParseValue("Dbtype", this.Dbtype)
	this.Dbname = this.ParseValue("Dbname", this.Dbname)
	this.Host = this.ParseValue("Host", this.Host)
	this.Port = this.ParseValue("Port", this.Port)
	this.Username = this.ParseValue("Username", this.Username)
	this.Password = this.ParseValue("Password", this.Password)
	return this
}
