package baseconfig

type SwaggerClientDto struct {
	IchubClientDto
	Host     string `json:"host"`
	BasePath string `json:"base_path"`
	Version  string `json:"version"`
	Title    string `json:"title"`
	Enable   string `json:"enable"`
}

func NewSwaggerClientDto() *SwaggerClientDto {
	return &SwaggerClientDto{}
}

func (this *SwaggerClientDto) Parse() *SwaggerClientDto {
	this.Host = this.ParseValue("Host", this.Host)
	this.BasePath = this.ParseValue("BasePath", this.BasePath)
	this.Version = this.ParseValue("Version", this.Version)
	this.Title = this.ParseValue("Title", this.Title)
	this.Enable = this.ParseValue("Enable", this.Enable)

	return this
}
