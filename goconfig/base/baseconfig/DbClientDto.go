package baseconfig

type DbClientDto struct {
	IchubClientDto

	Dbtype string `json:"dbtype"`
	Dbname string `json:"dbname"`
	Host   string `json:"host"`
	Port   string `json:"port"`

	Username string `json:"username"`
	Password string `json:"password"`
	Charset  string `json:"charset"`
	Sslmode  string `json:"sslmode"`
}

func NewDbClientDto() *DbClientDto {
	var ds = &DbClientDto{}
	ds.InitProxy(ds)
	return ds
}
func (this *DbClientDto) Parse() *DbClientDto {
	this.Dbtype = this.ParseValue("Dbtype", this.Dbtype)
	this.Dbname = this.ParseValue("Dbname", this.Dbname)
	this.Host = this.ParseValue("Host", this.Host)
	this.Port = this.ParseValue("Port", this.Port)
	this.Username = this.ParseValue("Username", this.Username)
	this.Password = this.ParseValue("Password", this.Password)
	return this
}
