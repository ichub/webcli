package baseconfig

import (
	"gitee.com/ichub/webcli/goconfig/base/baseutils/jsonutils"
)

/*
@Title    文件名称: RpcServerDto.go
@Description  描述: Rpc服务配置信息

@Author  作者: leijianming@163.com  时间(2024-02-18 22:38:21)
@Update  作者: leijianming@163.com  时间(2024-02-18 22:38:21)
*/
type RpcServerDto struct {
	EtcdHost   string `json:"etcd_host"`
	ServerName string `json:"server_name"`
	ServerPort int    `json:"server_port"`
	ClientName string `json:"client_name"`

	IchubClientDto
}

func NewRpcServerDto() *RpcServerDto {
	var dto = &RpcServerDto{}
	dto.InitProxy(dto)
	return dto
}

func (this *RpcServerDto) Parse() *RpcServerDto {
	this.EtcdHost = this.ParseValue("KEY", this.EtcdHost)
	this.ServerName = this.ParseValue("KEY", this.ServerName)
	this.ClientName = this.ParseValue("KEY", this.ClientName)

	return this
}

func (this *RpcServerDto) ToString() string {
	return jsonutils.ToJsonPretty(this)
}
