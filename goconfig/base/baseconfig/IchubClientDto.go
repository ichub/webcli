package baseconfig

import "gitee.com/ichub/webcli/goconfig/base/basedto"

type IchubClientDto struct {
	basedto.BaseEntity
}

func (this *IchubClientDto) ParseValue(key, value string) string {
	item := &ConfigItem{
		Key:   key,
		Value: value,
	}
	return item.ParseValue().EndValue
}
