package baseconfig

import (
	"gitee.com/ichub/webcli/goconfig/base/baseutils/jsonutils"
)

/*
@Title    文件名称: WebServerDto.go
@Description  描述: Web服务配置信息

@Author  作者: leijianming@163.com  时间(2024-02-18 22:38:21)
@Update  作者: leijianming@163.com  时间(2024-02-18 22:38:21)
*/
type WebServerDto struct {
	EtcdHost   string `json:"etcd_host"`
	ServerName string `json:"server_name"`
	ServerPort int    `json:"server_port"`
	//used by client
	//TestUrl string
	IchubClientDto
}

func NewWebServerDto() *WebServerDto {
	return &WebServerDto{}
}

func (this *WebServerDto) String() string {
	return jsonutils.ToJsonPretty(this)
}

func (this *WebServerDto) Parse() *WebServerDto {
	this.EtcdHost = this.ParseValue("KEY", this.EtcdHost)
	this.ServerName = this.ParseValue("KEY", this.ServerName)

	return this
}
