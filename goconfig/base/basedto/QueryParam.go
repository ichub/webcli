package basedto

import (
	"encoding/json"
	"gitee.com/ichub/webcli/goconfig/base/basemodel"
)

const CURRENT = 0
const PAGE_SIZE = 20

type QueryParam struct {
	Current    int     `json:"current"`
	PageSize   int     `json:"page_size"`
	OrderBys   *string `json:"order_bys"`
	FuzzyQuery bool    `json:"fuzzy_query"`
	EsQuery    bool    `json:"es_query"`

	Param basemodel.ParamBase `json:"param"`
}

func (param *QueryParam) String() string {
	s, _ := json.Marshal(param)
	return string(s)

}

func (param *QueryParam) ToString() string {
	s, _ := json.MarshalIndent(param, "", " ")
	return string(s)

}
