package basedto

import "fmt"

type IchubError struct {
	Code int
	Msg  string
}

func NewIchubError(code int, msg string) *IchubError {
	return &IchubError{Code: code, Msg: msg}
}

func NewIchubErrorServer() *IchubError {
	return NewIchubError(500, "fail")
}

func (ce *IchubError) Error() string {
	return fmt.Sprintf(ce.Msg+" :%d ", ce.Code)
}
