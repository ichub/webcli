package basedto

type IchubResultParams struct {
	Params map[string]interface{}
	BaseEntity
}

func NewIchubResultParams() *IchubResultParams {
	var p = &IchubResultParams{}
	p.InitProxy(p)
	return p
}
func (this *IchubResultParams) ValueOf(m map[string]interface{}) *IchubResultParams {
	this.Params = m
	return this
}
func (p *IchubResultParams) GetParams(key string) *IchubResultParams {
	params := NewIchubResultParams().ValueOf(p.Params[key].(map[string]interface{}))
	params.InitProxy(params)
	return params

}

func (p *IchubResultParams) GetParam(key string) *IchubResultParam {
	return NewIchubResultParam(p.Params[key])

}
func (p *IchubResultParams) GetMap(key string) map[string]interface{} {
	return p.Params[key].(map[string]interface{})

}

func (p *IchubResultParams) SetParam(key string, value interface{}) {
	p.Params[key] = value

}
