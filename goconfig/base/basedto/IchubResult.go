package basedto

import (
	"encoding/json"
	"fmt"
	"gitee.com/ichub/webcli/goconfig/base/baseutils/jsonutils"
	"gitee.com/ichub/webcli/goconfig/base/baseutils/stringutils"
	"gitee.com/ichub/webcli/goconfig/ichublog"
	"github.com/gookit/goutil/strutil"
	"github.com/imroc/req"
	"github.com/stretchr/testify/suite"
)

/*
	@Title    文件名称: ichubresult.go
	@Description  描述: 统一返回结构

	@Author  作者: leijianming@163.com  时间(2024-02-18 22:38:21)
	@Update  作者: leijianming@163.com  时间(2024-02-18 22:38:21)
*/

type IchubResult struct {
	BaseEntity
	Code int         `json:"code"`
	Msg  string      `json:"msg"`
	Data interface{} `json:"data,omitempty"`
}

func NewIchubResult() *IchubResult {
	var result = &IchubResult{}
	result.InitProxy(result)
	return result
}

func ParseIchubResult(body string) *IchubResult {

	var result IchubResult
	jsonutils.FromJson(body, &result)
	return &result
}

func IchubResultOf(resp *req.Resp) *IchubResult {
	var result IchubResult
	if resp.Response().StatusCode != 200 {
		return result.CodeMsg(resp.Response().StatusCode, "failed!")
	}
	jsonutils.FromJsonByte(resp.Bytes(), &result)
	return &result
}

func (result *IchubResult) FromJson(body []byte) (*IchubResult, error) {

	var err = json.Unmarshal(body, result)
	if err != nil {
		fmt.Println(err)

	}
	result.InitProxy(result)
	return result, err
}
func (result *IchubResult) CheckCode(suite suite.Suite, code int) {

	suite.Equal(code, result.Code)
}

// keyVal : "Pay=12"
func (result *IchubResult) Check(suite suite.Suite, keyVal string) {
	var kv = strutil.Split(keyVal, "=")
	suite.Equal(stringutils.Any2Str(result.ValueByKey(kv[0])), kv[1])
}

// keyVal : "Pay=12|l=2"
func (result *IchubResult) Checks(suite suite.Suite, keyVals string) {
	kvs := strutil.Split(keyVals, "|")
	for _, keyval := range kvs {
		result.Check(suite, keyval)
	}

}
func (result *IchubResult) DataIfMapParams() *IchubResultParams {

	var r = NewIchubResultParams()
	r.Params = result.Data.(map[string]interface{})
	r.InitProxy(r)
	return r
}

func (result *IchubResult) Log() {
	ichublog.IchubLog.Println(result.String())
}

func (result *IchubResult) ValueByKey(key string) interface{} {
	return result.Data.(map[string]interface{})[key]

}

func (result *IchubResult) CheckValueByKey(key string, expect interface{}) bool {
	var value = result.ValueByKey(key)

	v := stringutils.ToStringWith(value)
	exp := stringutils.ToStringWith(expect)
	return v == exp
}

func (result *IchubResult) ToString() string {
	s, _ := json.MarshalIndent(result, "", " ")
	return string(s)

}

func (result *IchubResult) Success() *IchubResult {
	result.Code = CODE_SUCCESS
	result.Msg = "成功"
	return result
}

func (result *IchubResult) SuccessData(data interface{}) *IchubResult {
	result.Code = CODE_SUCCESS
	result.Msg = "成功"
	result.Data = data
	return result
}
func (result *IchubResult) SuccessMessage(msg string, data interface{}) *IchubResult {
	result.Code = CODE_SUCCESS
	result.Msg = msg

	result.Data = data
	return result
}

func (result *IchubResult) Fail() *IchubResult {
	result.Code = CODE_FAIL
	result.Msg = "失败"
	return result
}

func (result *IchubResult) CodeMsg(code int, msg string) *IchubResult {
	result.Code = code
	result.Msg = msg
	return result
}

func (result *IchubResult) FailMessage(msg string) *IchubResult {
	result.Code = CODE_FAIL
	result.Msg = msg
	return result
}

func (p *IchubResult) SetData(s interface{}) {
	p.Data = s
}

func (p *IchubResult) GetData() interface{} {

	return p.Data
}

func (this *IchubResult) GetDbResult() map[string]interface{} {
	if this.Data == nil {
		return map[string]interface{}{}
	}
	var mapData = this.Data.(map[string]interface{})
	var dbResult = mapData["DbResult"].(map[string]interface{})
	return dbResult

}
func (this *IchubResult) GetEsResult() map[string]interface{} {
	if this.Data == nil {
		return map[string]interface{}{}
	}
	var mapData = this.Data.(map[string]interface{})
	var esResult = mapData["EsResult"].(map[string]interface{})
	return esResult

}
func (this *IchubResult) To(out interface{}) {
	var res = jsonutils.ToJsonPretty(this.Data)

	jsonutils.FromJson(res, &out)

}
func (this *IchubResult) EsResultTo(out interface{}) {
	var res = jsonutils.ToJsonPretty(this.GetEsResult()["data"])

	jsonutils.FromJson(res, out)

}
func (this *IchubResult) DbResultTo(out interface{}) {
	var res = jsonutils.ToJsonPretty(this.GetDbResult()["data"])

	jsonutils.FromJson(res, out)

}
