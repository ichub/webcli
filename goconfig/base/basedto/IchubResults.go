package basedto

import (
	"encoding/json"
	"fmt"
	"gitee.com/ichub/webcli/goconfig/base/baseutils/jsonutils"
	"gitee.com/ichub/webcli/goconfig/base/baseutils/stringutils"
	"gitee.com/ichub/webcli/goconfig/ichublog"
	"github.com/imroc/req"
	"github.com/stretchr/testify/suite"
)

/*
	@Title    文件名称: ichubresults.go
	@Description  描述: 批量统一返回结构

	@Author  作者: leijianming@163.com  时间(2024-02-18 22:38:21)
	@Update  作者: leijianming@163.com  时间(2024-02-18 22:38:21)
*/

type IchubResults struct {
	BaseEntity
	Code int           `json:"code"`
	Msg  string        `json:"msg"`
	Data []IchubResult `json:"data,omitempty"`
}

func ParseIchubResults(body string) *IchubResults {
	var result IchubResults
	jsonutils.FromJson(body, &result)
	return &result
}
func (results *IchubResults) CheckCode(suite suite.Suite, code int) {

	suite.Equal(code, results.Code)
}

func (results *IchubResults) Clone() *IchubResults {
	var newresult = *results
	return &newresult

}
func (results *IchubResults) FailCode(code int, msg string) *IchubResults {
	results.Code = code
	results.Msg = msg
	return results
}

func (results *IchubResults) Check(suite suite.Suite, exp, real interface{}) {

	suite.Equal(stringutils.Any2Str(exp), stringutils.Any2Str(real))
}

// func DataOfIndex(1)
func (results *IchubResults) FromJson(body []byte) (*IchubResults, error) {

	var err = json.Unmarshal(body, results)
	if err != nil {
		fmt.Println(err)

	}
	return results, err

}

func IchubResultsFromJson(body []byte) (*IchubResults, error) {
	var results IchubResults
	var err = json.Unmarshal(body, &results)
	if err != nil {
		fmt.Println(err)
		return &results, err
	}
	return &results, err
}

func (results *IchubResults) ValueOfData(index int) IchubResult {
	var value = (results.Data)[index]
	return value
}
func IchubResultsOf(resp *req.Resp) *IchubResults {
	var result IchubResults
	if resp.Response().StatusCode != 200 {
		return result.FailCode(resp.Response().StatusCode, "failed!")
	}
	jsonutils.FromJsonByte(resp.Bytes(), &result)
	return &result
}

func (results *IchubResults) String() string {
	return jsonutils.ToJsonPretty(results)

}

func (results *IchubResults) ToString() string {
	s, _ := json.MarshalIndent(results, "", " ")
	return string(s)

}
func (results *IchubResults) Success() *IchubResults {
	results.Code = CODE_SUCCESS
	results.Msg = "成功"
	return results
}

func (results *IchubResults) SuccessData(data []IchubResult) *IchubResults {
	results.Code = CODE_SUCCESS
	results.Msg = "成功"
	//results.Data = data
	return results
}
func (results *IchubResults) SuccessMessage(msg string, data []IchubResult) *IchubResults {
	results.Code = CODE_SUCCESS
	results.Msg = msg

	//results.Data = data
	return results
}
func NewIchubResults() *IchubResults {
	return &IchubResults{
		Data: []IchubResult{},
	}
}
func (results *IchubResults) Log() {

	fmt.Println(jsonutils.ToJsonPretty(results))
	ichublog.IchubLog.Println(jsonutils.ToJsonPretty(results))
}

func (results *IchubResults) Fail() *IchubResults {
	results.Code = CODE_FAIL
	results.Msg = "失败"
	return results
}
func (results *IchubResults) CodeMsg(code int, msg string) *IchubResults {
	results.Code = code

	results.Msg = msg
	return results
}
func (results *IchubResults) FailMessage(msg string) *IchubResults {
	results.Code = CODE_FAIL
	results.Msg = msg
	return results
}

func (results *IchubResults) SetData(data []IchubResult) {
	results.Data = data
}

func (results *IchubResults) GetData() []IchubResult {

	return results.Data
}
