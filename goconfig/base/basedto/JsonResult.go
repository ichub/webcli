package basedto

const CODE_SUCCESS = 200

const CODE_REQUEST_BAD = 400
const CODE_SERVER_ERR = 500
const CODE_NOFOUND_RECORD = 501

const CODE_FAIL = CODE_SERVER_ERR

type JsonResult struct {
	BaseEntity
	Code int    `json:"code"`
	Msg  string `json:"msg"`
	Data string `json:"data"`
}

func NewJsonResult() *JsonResult {
	return &JsonResult{}
}

func (result *JsonResult) initProxy() {
	if result.Proxy == nil {
		result.InitProxy(result)
	}

}

func (result *JsonResult) String() string {
	result.initProxy()
	return result.BaseEntity.String()

}

func (result *JsonResult) ToString() string {

	result.initProxy()
	return result.BaseEntity.ToString()

}

func (result *JsonResult) Success() *JsonResult {
	result.initProxy()
	result.Code = CODE_SUCCESS
	result.Msg = "成功"
	return result
}

func (result *JsonResult) SuccessData(data string) *JsonResult {
	result.initProxy()
	result.Code = CODE_SUCCESS
	result.Msg = "成功"
	result.Data = data
	return result
}
func (result *JsonResult) SuccessMessage(msg string, data string) *JsonResult {
	result.initProxy()
	result.Code = CODE_SUCCESS
	result.Msg = msg

	result.Data = data
	return result
}

func (result *JsonResult) Fail() *JsonResult {
	result.initProxy()
	result.Code = CODE_FAIL
	result.Msg = "失败"
	return result
}

func (result *JsonResult) FailMessage(msg string) *JsonResult {
	result.initProxy()
	result.Code = CODE_FAIL
	result.Msg = msg
	return result
}

func (p *JsonResult) SetData(s string) {
	p.Data = s
}

func (p *JsonResult) GetData() string {

	return p.Data
}
