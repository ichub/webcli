package basedto

import (
	"gitee.com/ichub/webcli/goconfig/base/baseutils/stringutils"
)

type BaseEntity struct {
	Proxy func() *BaseProxy `json:"-"`
}

func NewBaseEntity() *BaseEntity {
	var entity = &BaseEntity{}

	entity.Proxy = func() *BaseProxy {

		return NewBaseProxy(entity)
	}
	return entity
}
func (entity *BaseEntity) init() {
	if entity == nil {
		entity.InitProxy(entity)
	}
}

func (entity *BaseEntity) InitProxy(some interface{}) {
	entity.Proxy = func() *BaseProxy {
		return NewBaseProxy(some)
	}
}

func (entity *BaseEntity) String() string {
	entity.init()
	return entity.Proxy().String()
}

func (entity *BaseEntity) ToString() string {
	entity.init()
	return entity.Proxy().ToString()
}

func (entity *BaseEntity) Log() {
	entity.init()
	entity.Proxy().Log()

}

func (entity *BaseEntity) Clone() interface{} {
	entity.init()
	return entity.Proxy().Clone()
}

func (entity *BaseEntity) FromJson(body []byte) interface{} {
	entity.init()
	return entity.Proxy().FromJson(body)

}
func (entity *BaseEntity) ToJson() string {
	entity.init()
	return entity.Proxy().ToJson()

}

func (entity *BaseEntity) ToPrettyString() string {
	entity.init()
	return entity.Proxy().ToPrettyString()
}
func (this *BaseEntity) ValueFrom(from interface{}) {
	this.Proxy().ValueFrom(from)

}
func (this *BaseEntity) Id2Str(id int64) string {
	return stringutils.Any2Str(id)
}
