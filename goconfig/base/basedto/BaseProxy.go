package basedto

import (
	"fmt"
	//"github.com/huandu/go-clone"
	"gitee.com/ichub/webcli/goconfig/base/baseutils/jsonutils"
	"reflect"
)

type BaseProxy struct {
	Some interface{}
}

func NewBaseProxy(some interface{}) *BaseProxy {
	var proxy = new(BaseProxy)
	proxy.Some = some
	return proxy
}

func (proxy *BaseProxy) String() string {

	return jsonutils.ToJsonPretty(proxy.Some)
}

func (proxy *BaseProxy) ToString() string {

	return jsonutils.ToJsonPretty(proxy.Some)
}

func (proxy *BaseProxy) Log() {
	var name = reflect.TypeOf(proxy.Some).String()
	fmt.Println(name, " =", proxy.String())

}

func (proxy *BaseProxy) Clone() interface{} {
	return proxy.Some //clone.Clone(proxy.Some)

}

func (proxy *BaseProxy) FromJson(body []byte) interface{} {
	jsonutils.FromJsonByte(body, proxy.Some)
	return proxy.Some

}
func (proxy *BaseProxy) ToJson() string {
	return jsonutils.ToJsonPretty(proxy.Some)

}

func (proxy *BaseProxy) ValueOf(another interface{}) {
	proxy.Some = another
}
func (proxy *BaseProxy) ToPrettyString() string {
	return jsonutils.ToJsonPretty(proxy.Some)

}
func (Proxy *BaseProxy) ValueFrom(from interface{}) {
	//mapstructure.Decode(from, Proxy.Some)
	var fromstr = jsonutils.ToJsonPretty(from)
	jsonutils.FromJson(fromstr, Proxy.Some)

}
