package ichublog

import (
	"gitee.com/ichub/webcli/goconfig/base/baseconsts"
	"gitee.com/ichub/webcli/goconfig/base/baseutils"
	"gitee.com/ichub/webcli/goconfig/base/baseutils/jsonutils"
	"github.com/sirupsen/logrus"
)

var IchubLog = baseutils.NewIchubLogger(baseconsts.ICHUBENGINGE_LOG_FILE)

func Log(ps ...interface{}) {
	IchubLog.Println(ps)
	logrus.Info(ps)
}
func Log2PrettyStr(ps interface{}) {
	var res = jsonutils.ToJsonPretty(ps)
	Log(res)
}
func InitLogrus() {
	logrus.SetLevel(logrus.DebugLevel)
	//logrus.SetReportCaller(true)

	logrus.SetFormatter(&logrus.TextFormatter{
		ForceColors:               true,
		EnvironmentOverrideColors: true,
		TimestampFormat:           "2006-01-02 15:04:05", //时间格式
		FullTimestamp:             true,
		DisableLevelTruncation:    true,
	})

}
