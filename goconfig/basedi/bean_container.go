package basedi

import (
	"gitee.com/ichub/webcli/goconfig/base/baseiface"
	"github.com/sirupsen/logrus"
	"sync"
)

func RegisterLoadBean(name string, loadbean baseiface.LoadBean) {
	container.RegisterLoadBean(name, loadbean)
}

func RegisterBean(name string, bean baseiface.ISingleton) {
	container.RegisterBean(name, bean)
}

func FindBean(name string) baseiface.ISingleton {
	var beanInfo = container.FindBean(name)
	if beanInfo.bean.Single() {
		return beanInfo.Bean()
	}
	return beanInfo.CreateBean()
}

func FindBeanProxy(name string) baseiface.IbaseProxy {
	return FindBean(name).(baseiface.IbaseProxy)
}

type beanContainer struct {
	container map[string]*BeanInfo

	lock sync.RWMutex
}

var container = newBeanContainer()

func newBeanContainer() *beanContainer {
	return &beanContainer{
		container: make(map[string]*BeanInfo),
		lock:      sync.RWMutex{},
	}
}

func (this *beanContainer) FindBean(name string) *BeanInfo {
	this.lock.RLock()
	defer this.lock.RUnlock()

	var beanInfo, ok = this.container[name]
	if !ok {
		logrus.Error(name, " bean found?", ok)
		return nil
	}
	if beanInfo.bean == nil {
		beanInfo.SetBean(beanInfo.CreateBean())
	}
	return beanInfo
}

func (this *beanContainer) RegisterLoadBean(name string, loadbean baseiface.LoadBean) {
	this.lock.Lock()
	defer this.lock.Unlock()

	var beanInfo = NewBeanInfo()
	beanInfo.SetBeanName(name)
	beanInfo.SetLoadBean(loadbean)
	this.container[name] = beanInfo
}

func (this *beanContainer) RegisterBean(name string, bean baseiface.ISingleton) {
	this.lock.Lock()
	defer this.lock.Unlock()

	var beanInfo = NewBeanInfo()
	beanInfo.SetBeanName(name)
	beanInfo.SetBean(bean)
	this.container[name] = beanInfo
}

func (this *beanContainer) CreateBean(beanInfo *BeanInfo) baseiface.ISingleton {

	return beanInfo.CreateBean()
}
