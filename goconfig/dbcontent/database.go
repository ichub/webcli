package dbcontent

import (
	"gitee.com/ichub/webcli/goconfig/dbcontent/database"
	"gitee.com/ichub/webcli/goconfig/ichubconfig"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/sirupsen/logrus"
	"sync"
)

/*
	@Title    文件名称: database.go
	@Description  描述: dbclient客户端

	@Author  作者: leijianming@163.com  时间(2024-02-18 22:38:21)
	@Update  作者: leijianming@163.com  时间(2024-02-18 22:38:21)
*/

var DbContent = database.NewIchubDbClient()
var rw sync.RWMutex

func GetDB() *gorm.DB {

	if DbContent.Db == nil {
		rw.Lock()
		Ini()
		rw.Unlock()
	}
	return DbContent.DbDebug()
}

func Ini() *gorm.DB {
	var IchubConig = ichubconfig.NewConfig() //ichubconfig.IchubConfig
	var ds = IchubConig.ReadIchubDb()
	DbContent.ValueOf(ds)
	var err = DbContent.IniDb()
	if err != nil {
		logrus.Error(err)
	}
	return DbContent.DbDebug()

}
