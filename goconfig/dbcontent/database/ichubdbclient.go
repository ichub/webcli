package database

import (
	"fmt"
	"gitee.com/ichub/webcli/goconfig/base/baseconfig"
	"gitee.com/ichub/webcli/goconfig/base/baseconsts"
	"gitee.com/ichub/webcli/goconfig/base/basedto"
	"github.com/jinzhu/copier"
	"github.com/jinzhu/gorm"
	"github.com/sirupsen/logrus"
	"time"
)

/*
	@Title    文件名称: ichubclient.go
	@Description  描述: 代码工厂数据库配置信息
	@Author  作者: leijianming@163.com  时间(2024-03-13 22:38:21)
	@Update  作者: leijianming@163.com  时间(2024-03-13 22:38:21)
*/

type FuncGetDb func() *gorm.DB

type IchubDbClient struct {
	basedto.BaseEntity
	Db *gorm.DB `json:"-"`

	baseconfig.DbClientDto
}

func NewIchubDbClient() *IchubDbClient {
	var dbCfg = &IchubDbClient{}
	dbCfg.InitProxy(dbCfg)
	return dbCfg
}
func (this *IchubDbClient) DbDebug() *gorm.DB {
	return this.Db.Debug()
}

func (this *IchubDbClient) ValueOf(dto *baseconfig.DbClientDto) {
	copier.Copy(this, dto)
}

// DbUrl:"host=192.168.13.235 port=26257 user=code  password=123456  dbname=" + base.ModuleDefineInst.Dbname + " sslmode=require",
func (this *IchubDbClient) MakeDbUrl() (dburl string) {

	if this.Dbtype == baseconsts.DB_TYPE_MYSQL {
		return this.MakeDbUrlMysql()
	}
	if len(this.Password) > 0 {
		return this.MakeDbUrlPostgres()
	}
	return this.MakeDbUrlPostgres()
}

func (this *IchubDbClient) MakeDbUrlSsl() (dburl string) {

	if this.Dbtype == baseconsts.DB_TYPE_MYSQL {
		return this.MakeDbUrlMysql()
	}
	return this.MakeDbUrlPostgres()
}

func (this *IchubDbClient) MakeDbUrlMysql() string {
	return fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8&mb4&parseTime=True&loc=Local",
		this.Username,
		this.Password,
		this.Host,
		this.Port,

		this.Dbname)
}

func (this *IchubDbClient) MakeDbUrlPostgres() string {

	return fmt.Sprintf(
		"postgresql://%s:%s@%s:%s/%s?sslmode=disable",

		this.Username,
		this.Password,
		this.Host,
		this.Port,
		this.Dbname)

}

//DbUrl:        "host=192.168.13.235 port=26257 user=code  password=123456  dbname=" + base.ModuleDefineInst.Dbname + " sslmode=require",

func (this *IchubDbClient) MakeDbUrlCockRoach() string {
	//DbUrl:   ssmode=disable", require disable
	return fmt.Sprintf(
		"postgresql://%s:%s@%s:%s/%s?sslmode=%s",

		this.Username,
		this.Password,
		this.Host,
		this.Port,
		this.Dbname,
		this.Sslmode)

}

func (this *IchubDbClient) Log() (dburl string) {
	dburl = this.MakeDbUrl()
	logrus.Info(this.MakeDbUrl())
	return
}
func InitDbMysql(conn string) (*gorm.DB, error) {
	logrus.Info(conn)
	dbinst, err := gorm.Open(baseconsts.DB_TYPE_MYSQL, conn)
	if err != nil {
		logrus.Error("Failed to connect mysql database, err:", err.Error(), conn)
		return nil, err
	}
	//dbinst.AutoMigrate(&cmsmodel.CmsColumn{})
	dbinst.DB().SetMaxOpenConns(50)
	dbinst.DB().SetMaxIdleConns(10)
	dbinst.DB().SetConnMaxLifetime(time.Minute * 60)

	return dbinst, err
}

func InitDbPostgres(conn string) (dbinst *gorm.DB, err error) {

	dbinst, err = gorm.Open(baseconsts.DB_TYPE_POSTGRES, conn)
	if err != nil {
		logrus.Error("InitDbPostgres 连接postgres失败!", conn, err)
		return nil, err
	}
	if err = dbinst.DB().Ping(); err != nil {
		return dbinst, err
	}
	//dbinst.AutoMigrate(&cmsmodel.CmsColumn{})
	dbinst.DB().SetMaxOpenConns(50)
	dbinst.DB().SetMaxIdleConns(10)
	dbinst.DB().SetConnMaxLifetime(time.Minute * 60)

	return dbinst, err
}

func (this *IchubDbClient) GetDb() *gorm.DB {
	return this.Db
}

func (this *IchubDbClient) Valueof(ds *baseconfig.DbClientDto) {
	copier.Copy(this, ds)
}

func (this *IchubDbClient) IniDb() error {
	var err error
	var dbtype = this.Dbtype
	if dbtype == baseconsts.DB_TYPE_MYSQL {
		this.Db, err = InitDbMysql(this.MakeDbUrlMysql())
	}

	if dbtype == baseconsts.DB_TYPE_POSTGRES {
		this.Db, err = InitDbPostgres(this.MakeDbUrlPostgres())

	}

	if dbtype == baseconsts.DB_TYPE_COCKROACH {
		this.Db, err = InitDbPostgres(this.MakeDbUrlCockRoach())
		if err != nil {
			logrus.Error(err)
		}
	}
	return err
}
