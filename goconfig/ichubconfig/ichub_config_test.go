package ichubconfig

import (
	"gitee.com/ichub/webcli/goconfig/base/baseconsts"
	"gitee.com/ichub/webcli/goconfig/base/baseutils/fileutils"
	"gitee.com/ichub/webcli/goconfig/ichublog"
	"github.com/sirupsen/logrus"
	"os"
	"testing"
)

func init() {
	ichublog.InitLogrus()
	fileutils.FindRootDir()

}
func Test001_LogString(t *testing.T) {
	var cfg = NewConfig()
	cfg.Read()

	logrus.Info(os.Getenv(baseconsts.IchubBasePath))
}
