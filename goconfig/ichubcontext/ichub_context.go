package ichubcontext

import (
	"gitee.com/ichub/webcli/goconfig/base/basedto"
	"gitee.com/ichub/webcli/goconfig/base/baseutils/fileutils"
)

var CommonContext *ichubCommonContext = newIchubCommonContext()

const (
	INPUT_RULE_PATH  = "/data/input/rule/"
	OUTPUT_RULE_PATH = "/data/output/rule/"
)

type ichubCommonContext struct {
	basedto.BaseEntity

	BasePath    string
	OutRulePath string
	InRulePath  string
}

func newIchubCommonContext() *ichubCommonContext {
	var ctxt = &ichubCommonContext{}

	ctxt.BasePath = fileutils.FindRootDir()
	ctxt.InRulePath = ctxt.BasePath + INPUT_RULE_PATH
	ctxt.OutRulePath = ctxt.BasePath + OUTPUT_RULE_PATH

	ctxt.InitProxy(ctxt)
	return ctxt
}

func (this *ichubCommonContext) WriteDaoFile(daofile string, content string) error {
	//	fileutil.RemoveFile(this.DaoFilePath + daofile)
	//	err := fileutil.WriteStringToFile(this.DaoFilePath+daofile, content, false)
	//	if err != nil {
	//		logrus.Error(err)
	//	}
	return nil
}
