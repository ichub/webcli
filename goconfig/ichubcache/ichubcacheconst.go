package ichubcache

import "time"

const cache_expire_time = 30 * time.Minute
const cache_clean_time = 5 * time.Minute
