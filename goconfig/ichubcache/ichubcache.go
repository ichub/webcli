package ichubcache

import "github.com/patrickmn/go-cache"

import "time"

type IchubCache struct {
	gocache    *cache.Cache
	Pkey       string
	ExpireTime time.Duration
}

func NewCache() *IchubCache {
	return NewIchubCache("ichub:")
}
func NewIchubCache(Pkey string) *IchubCache {
	return &IchubCache{
		Pkey:       Pkey,
		ExpireTime: 30 * time.Second,
		// 默认过期时间10s；清理间隔30s，即每30s钟会自动清理过期的键值对
		gocache: cache.New(30*time.Second, 60*time.Second),
	}
}

func (this *IchubCache) Set(k string, x interface{}, d time.Duration) {
	this.gocache.Set(this.Pkey+k, x, d)
}

func (this *IchubCache) Get(k string) (interface{}, bool) {
	return this.gocache.Get(this.Pkey + k)
}
