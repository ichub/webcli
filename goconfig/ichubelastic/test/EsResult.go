package main

import "time"

type T struct {
	Took int `json:"took"`
	Hits struct {
		Total struct {
			Value    int    `json:"value"`
			Relation string `json:"relation"`
		} `json:"total"`
		MaxScore float64 `json:"max_score"`
		Hits     []struct {
			Score       float64     `json:"_score"`
			Index       string      `json:"_index"`
			Type        string      `json:"_type"`
			Id          string      `json:"_id"`
			SeqNo       interface{} `json:"_seq_no"`
			PrimaryTerm interface{} `json:"_primary_term"`
			Source      struct {
			} `json:"_source"`
		} `json:"hits"`
	} `json:"hits"`
	Shards struct {
		Total      int `json:"total"`
		Successful int `json:"successful"`
		Failed     int `json:"failed"`
	} `json:"_shards"`
}
type T2 struct {
	Index       string `json:"_index"`
	Type        string `json:"_type"`
	Id          string `json:"_id"`
	Uid         string `json:"_uid"`
	Routing     string `json:"_routing"`
	Parent      string `json:"_parent"`
	Version     int    `json:"_version"`
	SeqNo       int    `json:"_seq_no"`
	PrimaryTerm int    `json:"_primary_term"`
	Source      struct {
		User     string    `json:"user"`
		Message  string    `json:"message"`
		Retweets int       `json:"retweets"`
		Created  time.Time `json:"created"`
	} `json:"_source"`
	Found bool `json:"found"`
}
